ad_page_contract {

    event management registration link

} {

} 

set error_text "test"
set validate_token ""

db_1row event_info "select event_url, event_email, event_name from event_management_events WHERE project_id = 67700"
set base_url [export_vars -base "[ad_url]"]

set count 0 

db_foreach participants "SELECT c.email as email, ep.event_participant_status_id as status_id, c.first_names as first_names, ep.participant_id as participant_id, c.person_id as person_id FROM im_event_participants ep INNER JOIN cc_users c ON (c.user_id = ep.person_id) WHERE project_id = 67700 AND course in (35117,61195) AND event_participant_status_id in (82502, 82503, 82504)" {
	set validate_token [ns_sha1 K3KSrvQI-${participant_id}]
	#db_1row user_info "select first_names, last_name from persons where person_id = :participant_id"
    set url [export_vars -base "[ad_url][apm_package_url_from_key "intranet-event-management"]participant-preferences" -url {participant_id {token $validate_token}}]
	set mail_subject "Choose your class preference for Swing Castle Camp"
	set send_email_to "$email"

	set mail_body "Hello $first_names, <br/><br/>Only two days to go till Swing Castle Camp, I hope you are as excited as we are. You are registered for the swing track for which we offer three different choices. Please select your preference by clicking this link:<br/> <a href='$url'>$url</a><br/><br/>See you very soon<br/>SCC team"
#	acs_mail_lite::send -send_immediately -to_addr $send_email_to -from_addr $event_email -use_sender -subject $mail_subject -body $mail_body -mime_type "text/html" 

	incr count
}

ad_return_error "ok" "$count emails sent"
