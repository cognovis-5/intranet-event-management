<master>
<property name="title">@page_title;noquote@</property>
<property name="context">@context;noquote@</property>

@context_bar;noquote@
<p>
<div>
    <a class="button" href="event-one">#intranet-event-management.Add_Event#</a>
    <a class="button" href="rooms-list">#intranet-event-management.Rooms#</a>
    <a class="button" href="rooms-cross-event-list">Cross Event List</a>
    <a class="button" href="invite-participants">#intranet-event-management.Invite_Participants#</a>
</div>
<br>
<br>
<listtemplate name="events_list"></listtemplate>
<p />
<multiple name="events">
    <include src="/packages/intranet-event-management/lib/event-stats" project_id=@events.project_id@>
</multiple>
<br>
<br>
#intranet-event-management.Provider_Company# <a href="@provider_company_link@">@provider_company_name@</a>
