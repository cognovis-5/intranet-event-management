<master>
<property name="title">@page_title@</property>
<property name="context"></property>
<property name="main_navbar_label">#intranet-event-management.Rooms#</property>
<p />
<style>
input[type=checkbox] {
  transform: scale(1.5);
}
</style>
<listtemplate name="checkin_list"></listtemplate>
