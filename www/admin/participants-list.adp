<master>
<property name="title">@page_title@</property>
<property name="context"></property>
<property name="main_navbar_label">#intranet-event-management.participants#</property>
<property name="left_navbar">@left_navbar_html;noquote@</property>
<property name="show_context_help">@show_context_help_p;noquote@</property>

<SCRIPT Language=JavaScript src=/resources/diagram/diagram/diagram.js></SCRIPT>

    @context_bar;noquote@
    <p>
    <a class="button" href="registration?project_id=@project_id@">#intranet-event-management.add_participant#</a>
    <!-- <a class="button" href="other-room-assign?project_id=@project_id@">#intranet-event-management.other_room_assign#</a>-->
    <a class="button" href="participant-checkin?project_id=@project_id@">Checkin Liste</a>
    <a class="button" href="lottery?project_id=@project_id@">Run Lottery</a>
    <a class="button" href="generate-csv?project_id=@project_id@">CSV</a>
    <p>
    <include src="/packages/intranet-event-management/lib/event-stats" project_id=@project_id@ event_participant_level_id=@event_participant_level_filter_id@>
	<table class="table_list_page">
            @table_header_html;noquote@
            @table_body_html;noquote@ 
	</table>

