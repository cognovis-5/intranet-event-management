ad_page_contract {
    @author malte.sussdorff@cognovis.de
    Send participants an E-Mail with a link to the sencha portal
    
} {
    {participant_ids ""}
    {project_id ""}
    {return_url ""}
}

# Basic information
db_1row context "select event_email,event_name from event_management_events where project_id = :project_id"

set recipients [list]
set locale_list [list]

foreach participant_id $participant_ids {
    if {![empty_string_p $participant_id]} {
	set party_id [db_string party_id "select person_id from im_event_participants where participant_id = :participant_id"]
	lappend recipients [list "[party::name -party_id $party_id]</a> ([cc_email_from_party $party_id])" $participant_id]

	# Check the locale. Use the locale of all selected users if the locale is identical
	set party_locale [lang::user::locale -user_id $party_id]
	if {[lsearch $locale_list $party_locale]} {
	    lappend locale_list $party_locale
	}
    }
}

if {[llength $locale_list] eq 1} {
    set message_locale [lindex $locale_list 0]
} else {
    set message_locale ""
}

# Primary from should be the mail sender of the event
set from_addr $event_email
set from_options [list [list $from_addr $from_addr]]

# Append the current logged in user
set current_user_id [auth::get_user_id]
if {$current_user_id >0} {
    db_1row from_info "select im_name_from_id(party_id) as name, email from parties where party_id = :current_user_id"
    lappend from_options [list $name $email]
}

# Use the URL to figure out if the portal is configured
set sencha_registration_app_url [parameter::get_from_package_key -package_key "intranet-event-management" -parameter "SenchaRegistrationAppUrl" -default ""]

set mime_type "text/html"

set form_id "participant-mail"
set action_url "participant-mail"
ad_form \
    -name $form_id \
    -html { enctype multipart/form-data } \
    -action $action_url \
    -export {return_url project_id} \
    -form {
	{to:text(checkbox),multiple,optional
	    {label "[_ acs-mail-lite.Recipients]:"} 
	    {options  $recipients }
	    {html {checked 1}}
	}
	{recipients:text(hidden)
	    {value $recipients}
	}
	{to_addr:text(text),optional
	    {label "[_ acs-mail-lite.Recipients]:"} 
	    {html {size 56}}
	    {help_text "[_ acs-mail-lite.cc_help]"}
	}
	{from_addr:text(select)
	    {label "[_ acs-mail-lite.Sender]"}
	    {value $from_addr}
	    {options $from_options}
	}
	{subject:text(text)
	    {label "[_ acs-mail-lite.Subject]"}
	    {html {size 55}}
	}
	{content_body:text(richtext),optional
	    {label "[_ acs-mail-lite.Message]"}
	    {html {cols 100 rows 30}}
	}
	{upload_file:file(file),optional
	    {label "[_ acs-mail-lite.Upload_file]"}
	}
    } -on_request {

	# Set the body up with a standard template
	# This should contain @salutation_pretty;noquote@ and the @link;noquote@ pointing
	# to the sencha configured portal

	set subject [lang::message::lookup $message_locale intranet-event-management.email_subject_default "%event_name%: "]
	set content_body [lang::message::lookup $message_locale intranet-event-management.email_body_default "@salutation_pretty;noquote@<p />"]
	if {$sencha_registration_app_url ne ""} {
	    append content_body "<p>[lang::message::lookup "" intranet-event-management.email_app_link_default "<a href='@link;noquote@'>Visit your registration</a><p />"]</p>"
	}

	set content_body [template::util::richtext::create $content_body "html"]
    } -on_submit {

	set signature [db_string signature "select signature from parties where email = :from_addr" -default ""]
	if {$signature ne ""} {
	    append content_body [template::util::richtext::get_property html_value $signature]
	}
	
	
        # List to store know wich emails recieved the message
        set recipients_addr [list]
	
        set to_addr [split $to_addr ";"]
	
        # Insert the uploaded file linked under the package_id
        set package_id [ad_conn package_id]
        
        if {![empty_string_p $upload_file] } {
            set revision_id [content::item::upload_file \
                                 -package_id $package_id \
                                 -upload_file $upload_file \
                                 -parent_id $party_id]
        }

        # Handle the file ids
	set file_ids [list]
	
        if {[exists_and_not_null revision_id]} {
            if {[exists_and_not_null file_ids]} {
                append file_ids " $revision_id"
            } else {
                set file_ids $revision_id
            }
        }
	
        foreach participant_id $to {
	    set party_id [db_string party_id "select person_id from im_event_participants where participant_id = :participant_id"]
            set email [cc_email_from_party $party_id]

	    # set the participant array based on
	    # the email
	    set participant_arr($email) $participant_id
	    set party_arr($email) $party_id
	    
            # Check if the cc_ids is already there
            if {[lsearch $to_addr $email]<0} {
                lappend to_addr $email
            }
        }
	
	
	set to_addr [lsort -unique $to_addr]

	set object_id $project_id

	foreach to $to_addr {
	    
	    # Make sure to have an empty link by default
	    set link ""

	    # Handle unknown emails
	    if {[exists_and_not_null party_arr($to)]} {

		set recipient_id $party_arr($to)
		#set salutation_pretty [ad_call_proc_if_exists im_invoice_salutation -person_id $recipient_id]
		set first_names [db_string get_first_names "select first_names from persons where person_id=:recipient_id" -default ""]
		if {$first_names ne ""} {
		    set salutation_pretty "Dear $first_names"
		} else {
		    set salutation_pretty "Hi,"
		}
		
		# check if the recipient is a participant
		# If yes, set the object_id to the participation
		if {[exists_and_not_null participant_arr($to)]} {
		    set participant_id $participant_arr($to)
		    set object_id $participant_id

		    # If we have a participant, we can generate the url
		    if {$sencha_registration_app_url ne ""} {
			set token [im_generate_auto_login -user_id $recipient_id]
			set link [export_vars -base "$sencha_registration_app_url/" -url {{page "waiting-spot"} {ws_token "$token"} {ws_user_id "$recipient_id"} {project_id "$project_id"}}]
		    }

		}

	    } else {
		set salutation_pretty "Hi,"
	    }

	    # Parse the body. This will replace all
	    # @...@ variables
	    eval [template::adp_compile  -string "$content_body"]
	    set out_body $__adp_output
	    
	    eval [template::adp_compile -string "$subject"]
	    set subject $__adp_output
	    
		set chilkat_id [intranet_chilkat::send_mail \
            -to_addr $to \
            -from_addr $from_addr \
            -reply_to $from_addr \
            -subject $subject \
            -body $out_body \
			-file_ids $file_ids \
            -object_id $object_id]
	}
	
    } -after_submit {
	ad_returnredirect "$return_url"
    }
