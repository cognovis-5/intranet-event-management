ad_page_contract {
    
    Allow to reply to a logged e-mail
    
    @author michaldrn@wp.pl
    @creation-date 2019-10-06
    @cvs-id $Id$
} {
    project_id
} 


# Header for CSV file
set csv_rows "\"Sort Order\",\"Reg.ID\",\"Name\",\"E-Mail\",\"Event\",\"Dance Role\",\"Food Choice\",\"Dance Partner\",\"Dance Partner Name\",\"Status\",\"Country\",\"Notes\",\"Level\",\"Payment\"\n"

set sql_query "select ep.person_id as person_id, sort_order, participant_id, concat(u.first_names, ' ', u.last_name) as name, (select material_name from im_materials where material_id = ep.course) as event, (select category from im_categories where category_id = ep.event_participant_type_id) as dance_role, (select material_name from im_materials where material_id = ep.food_choice) as food_choice, (select category from im_categories where category_id = ep.event_participant_status_id) as status,  im_event_partners__html(:project_id,ep.participant_id,'') as dance_partner,u.email, im_name_from_id(payment_type_id) as payment_type,im_name_from_id(event_participant_level_id) as dance_level from im_event_participants ep, cc_users u where u.user_id = ep.person_id and project_id=:project_id order by sort_order asc"


set count 0
db_foreach download_info_select $sql_query {
    # Dance partner needs to be stripped off html tags
    regsub -all {<br />} $dance_partner "\n" dance_partner

    regsub -all {<[^>]*>} $dance_partner {} dance_partner 
	set country [db_string get_country "select country_name from country_codes cc, users_contact uc where uc.ha_country_code = cc.iso and uc.user_id = :person_id" -default ""]


    set notes_pretty_list [list]
    set dance_partner_name [db_string dp_name "select event_partner_name from im_event_partners where participant_id = :participant_id limit 1" -default ""]
    # Find out if the participant has provided a note
	db_foreach note {select note, im_name_from_id(note_type_id) as note_type from im_notes where object_id = :participant_id} {
	    lappend notes_pretty_list "<b>$note_type</b>: [string trim $note]"
	}

    set notes_pretty [join $notes_pretty_list "<br /><br />"]
    append csv_rows "\"$sort_order\",\"$participant_id\",\"$name\",\"$email\",\"$event\",\"$dance_role\",\"$food_choice\",\"$dance_partner\",\"$dance_partner_name\",\"$status\",\"$country\",\"$notes_pretty\",\"$dance_level\",\"$payment_type\"\n"
    incr count
}

if { $count == 0 } {
    doc_return 200 text/plain "No users meet your criteria"
} else {
    ad_return_string_as_file -mime_type text/csv -string $csv_rows -filename "participants.csv"
}


