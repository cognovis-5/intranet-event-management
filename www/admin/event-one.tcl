ad_page_contract {
    
    event management event add/edit/view page
    
    @creation-date 2014-10-15
    @last-modified 2014-10-30
    @cvs-id $Id$
} {
    project_id:integer,optional,notnull
    project_type_id:integer,optional,notnull
} -properties {
} -validate {
} -errors {
}

if {![info exists project_type_id]} {
    set project_type_id [im_id_from_category "Event" "Intranet Project Type"]
}

set page_title "Event Form"
set context ""
set context_bar [ad_context_bar $page_title]

set form_id "event_form"
set action_url ""
set object_type "event_management_event" ;# used for appending dynfields to form

if { [exists_and_not_null project_id] } {
    set mode display
} else {
    set mode edit
}
# TODO: disable cost_center_id when editing form
ad_form \
    -name $form_id \
    -action $action_url \
    -mode $mode \
    -form {

        project_id:key(acs_object_id_seq)
        {-section event_info 
            {legendtext {[::event_management::mc Event_Info "Event Info"]}}}

        {event_name:text
            {label {[::event_management::mc Event_Name "Name"]}}}

        {event_url:text
            {label {[::event_management::mc Event_URL "URL"]}}}
                
        {event_email:text
            {label {[::event_management::mc Event_E-Mail "Sender E-Mail"]}}
        }

        {start_date:date(date)
            {label {[::event_management::mc Event_Start_date "Start Date"]}}
            {format "YYYY-MM-DD"} 
            {after_html {<input type="button" style="height:23px; width:23px; background: url('/resources/acs-templating/calendar.gif');" onclick ="return showCalendarWithDateWidget('start_date', 'y-m-d');" >}}
        }
	
	{project_status_id:text(im_category_tree)
	    {label {Project Status}} 
	    {custom {category_type "Intranet Project Status" translate_p 1 package_key "intranet-event-management"}}
        }

	{project_type_id:text(im_category_tree)
	    {label {Project Type}} 
	    {custom {category_type "Intranet Project Type" translate_p 1 package_key "intranet-event-management"}}
        }

        {project_cost_center_id:text(generic_sql)
            {label {[::event_management::mc Project_Cost_Center "Project Cost Center"]}}
            {html {}}
            {custom {sql {select cost_center_id,cost_center_code || ' - ' || cost_center_name from im_cost_centers}}}}

    }


set dynfield_project_id 0
if {[exists_and_not_null project_id]} {
    db_0or1row project_id "select p.project_id, project_type_id from im_projects p where project_id = :project_id"
    set dynfield_project_id $project_id
}

 im_dynfield::append_attributes_to_form \
    -object_type im_project \
    -form_id $form_id \
    -object_subtype_id $project_type_id \
    -object_id $dynfield_project_id \
    -advanced_filter_p 0 

set section ""

set elements [list]

set sql "
    select material_name, currency, price, material_type, m.material_id, m.material_type_id, im_name_from_id(uom_id) as uom_name 
    from im_timesheet_prices itp 
    inner join im_materials m on (m.material_id=itp.material_id) 
    inner join im_material_types mt on (m.material_type_id=mt.material_type_id) 
    where itp.task_type_id = :project_type_id and company_id = [im_company_internal]
    order by mt.material_type_id,itp.uom_id,material_name	     
"


db_foreach material_id $sql {

    if { $section ne $material_type_id } {

        set section $material_type_id

        set legendtext "Capacity for \"$material_type\" materials"

        lappend elements [list -section $material_type_id [list legendtext $legendtext]]

    }


    set varname "capacity.${material_id}"

    lappend elements \
        [list ${varname}:text,optional \
            [list label "${material_name}"] \
            [list html "size 15"] \
            [list help_text "[format "%.2f" ${price}] $currency / ${uom_name}"]]

}


ad_form -extend -name $form_id -form $elements -validate {

    {event_name 
        {[db_string must_not_exist "select false from im_projects where project_name=:event_name and project_id != :project_id" -default true]}
        {[::event_management::mc event_name_exists "event name must be unique, given name already exists"]}}

}

ad_form -extend -name $form_id -edit_request {

    set sql "
        select *
        from event_management_event_materials
        where project_id=:project_id
    "

    db_foreach material_capacity $sql {
        set varname capacity.${material_id}
        set $varname $capacity
    }

    set sql "
        select evt.*,p.project_id,p.project_cost_center_id,project_name as event_name, project_status_id, project_type_id, 
	to_char(p.start_date,'YYYY-MM-DD') as ansi_start_date
        from event_management_events evt
        inner join im_projects p on (p.project_id = evt.project_id)
        where p.project_id = :project_id
    "

    db_1row event_info $sql
    set start_date [template::util::date::from_ansi $ansi_start_date "YYYY-MM-DD"]
    
} -new_data {

    db_transaction {

        set provider_company_id 8720  ;# Flying Hamburger

        set project_nr [im_next_project_nr]

        set sql "
            select event_management_event__new(
                :project_id,
                :event_name,
                :provider_company_id,
                :project_nr,
                :project_cost_center_id,
		:event_url,
		:event_email
            );
        "

        db_exec_plsql insert_event $sql
        
        # The form has an input field (named capacity.material_id) 
        # for each material of the following types: Course Income,
        # Accomodation, Food Choice, Bus Options.

        foreach varname [info vars capacity.*] {

            set capacity [set $varname]

            set material_id [lindex [split $varname {.}] 1]

            set sql "
                insert into event_management_event_materials (
                    project_id,
                    material_id,
                    capacity
                ) values (
                    :project_id,
                    :material_id,
                    :capacity
                )
            "

            db_dml insert_material_capacity $sql

        }

    }

} -edit_data {

    db_transaction {
        
        set sql "update im_projects set project_name = :event_name, project_status_id = :project_status_id,
	   project_type_id = :project_type_id where project_id = :project_id
        "

        db_dml update_project $sql

        set sql "update event_management_events set event_url = :event_url , event_email = :event_email where project_id = :project_id
        "

        db_dml update_event $sql

        # Note that we have to preserve the number of occupied slots while
        # updating the capacity of each material for the given event, or
        # disallow updating the capacity once we start accepting regisrations
        # for the event.
        
        set sql "delete from event_management_event_materials where project_id=:project_id"

        db_dml delete_old_capacity_rows $sql

	db_dml update_project "update im_projects set project_type_id = :project_type_id where project_id = (select project_id from event_management_events where project_id = :project_id)"

        # The form has an input field (named capacity.material_id) 
        # for each material of the following types: Course Income,
        # Accomodation, Food Choice, Bus Options.

        foreach varname [info vars capacity.*] {

            set capacity [set $varname]

            set material_id [lindex [split $varname {.}] 1]

            set sql "
                insert into event_management_event_materials (
                    project_id,
                    material_id,
                    capacity
                ) values (
                    :project_id,
                    :material_id,
                    :capacity
                )
            "

            db_dml insert_material_capacity $sql

        }

    }

} -after_submit {

    # Store the dynfields
    im_dynfield::attribute_store \
	-object_id $project_id \
	-form_id $form_id

    # Update url and e-mail
    db_dml update "update event_management_events set event_url = :event_url, event_email = :event_email where project_id = :project_id"
    set start_date_sql [template::util::date get_property sql_date $start_date]
    db_dml update "update im_projects set start_date = $start_date_sql where project_id = (select project_id from event_management_events where project_id = :project_id)"

    ad_returnredirect [export_vars -base event-one {project_id}]

}


