ad_page_contract {

    @author Neophytos Demetriou (neophytos@azet.sk)
    @creation-date 2014-11-02
    @last-modified 2014-11-14

    Confirmed & Confirmation E-Mail:

    * If we save the registrant in the list page or the edit page and 
      put him/her to confirmed, send out an E-Mail to the registrant 
      that they have a spot for the Camp. The E-Mail should contain 
      what they signed up for as well as a link to get to the payment 
      page for that registration

    * Once we confirm the user, a Quote (invoice type) is created in 
      the system based on the materials used for the registration. This 
      basically is the transformation of the registration into a financial 
      document. We can talk a little bit more about this in skype.

} {
    project_id:integer,notnull
    participant_ids:notnull
    return_url:trim,notnull
} -validate {
    check_confirmed_free_capacity -requires {project_id:integer} {
        ::event_management::check_confirmed_free_capacity -project_id $project_id -participant_id_list $participant_ids
    }
}
db_1row event_info "select project_cost_center_id, p.project_id, f.* from event_management_events f, im_projects p where p.project_id = :project_id and p.project_id = f.project_id"

    foreach id $participant_ids {

	set invoice_id [db_string invoice "select invoice_id from im_event_participants where participant_id = :id" -default 0]
	if {$invoice_id} {
	    # An E-Mail is send to the participant with the PDF attached and the payment 
	    # information similar to what is displayed on the Web site.
	    ::event_management::send_invoice_mail -invoice_id $invoice_id -from_addr $event_email -project_id $project_id
	}        
    }

ad_returnredirect $return_url


