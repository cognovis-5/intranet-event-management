ad_page_contract {
    
    Allow to reply to a logged e-mail
    
    @author michaldrn@wp.pl
    @creation-date 2019-10-06
    @cvs-id $Id$
} {
    { num_spots ""}
    project_id
} 

set lottery_statistics_html ""
set lottery_statistics_html_list [list]

# Main check if lottery was run
set lottery_was_executed_before_p [db_string get_lottery_executed_p "select lottery_run_p from event_management_events where project_id =:project_id" -default 0]

if {$lottery_was_executed_before_p eq "t"} {
    set lottery_result "Lottery was already executed"
} else {
   set lottery_result "Lottery not executed yet"
}

set lottery_statistics_sql "select count(participant_id) as total, event_participant_status_id as status_id from im_event_participants  group by event_participant_status_id"

db_foreach lottery $lottery_statistics_sql {
    append lottery_statistics_html "<br/> Total <b>[im_name_from_id $status_id]</b>: $total"
}

#set lottery_statistics_html [join $lottery_statistics_html_list "/n"]


set lottery_executed_p 0
set affected_participants [list]
set total_affected_participants 0
set page_tite Lottery
set page_title "Run Lottery"
set form_id "lottery"


ad_form \
    -name $form_id \
    -form {
        {project_id:text(hidden) {value $project_id}}
        {num_spots:text
            {label {[_ intranet-event-management.NumSpots]}}
            {html {size 10}}
	    {help_text {[_ intranet-event-management.NumSpotsHelp]}}
        }
    } -on_submit {
        
        if {$num_spots} {
            set confirm_p 1
        } else {
            set confirm_p 0
        }

        set total_affected_participants [event_management_run_lottery -num_spots $num_spots -project_id $project_id -randomize_p 1 -confirm_p $confirm_p]
        set lottery_executed_p 1
        
        ad_returnredirect [export_vars -base "participants-list" -url {project_id}]

    }


#if {$total_affected_participants > 0} {
 #   set lottery_result "Lottery executed! total number of affected participants: $total_affected_participants"
#} else {
 #   if {$lottery_executed_p} {
  #      set lottery_result "Lottery was executed, but didn't affect any participant"
   # } else {
    #    set lottery_result "Lottery not executed yet"
    #}
#}





