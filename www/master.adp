
<!DOCTYPE html>
<html lang="de-DE">
<head>
<meta charset='UTF-8'>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<title>Register | Balboa in the Attci</title>
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Balboa in the Attic &raquo; Feed" href="https://balboaintheattic.de/feed/" />
<link rel="alternate" type="application/rss+xml" title="Balboa in the Attic &raquo; Kommentar-Feed" href="https://balboaintheattic.de/comments/feed/" />
        <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/balboaintheattic.de\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9"}};
            !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56794,8205,9794,65039],[55358,56794,8203,9794,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
        </script>
        <style type="text/css">
img.wp-smiley,
img.emoji {
    display: inline !important;
    border: none !important;
    box-shadow: none !important;
    height: 1em !important;
    width: 1em !important;
    margin: 0 .07em !important;
    vertical-align: -0.1em !important;
    background: none !important;
    padding: 0 !important;
}
</style>
<link rel='stylesheet' id='bootstrap-css'  href='https://balboaintheattic.de/wp-content/themes/hestia-pro/assets/bootstrap/css/bootstrap.min.css?ver=1.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='https://balboaintheattic.de/wp-content/themes/hestia-pro/assets/font-awesome/css/font-awesome.min.css?ver=1.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='hestia_style-css'  href='https://balboaintheattic.de/wp-content/themes/hestia-pro/style.css?ver=1.1.52' type='text/css' media='all' />
<style id='hestia_style-inline-css' type='text/css'>

.header-filter-gradient { 
    background: linear-gradient(45deg, rgba(233,30,99,0.88) 0%, rgba(233,30,99,0.31) 100%); 
} 

.header-filter.header-filter-gradient:before {
    background-color: transparent;
}     
a, 
.navbar .dropdown-menu li:hover > a,
.navbar .dropdown-menu li:focus > a,
.navbar .dropdown-menu li:active > a,
.navbar .dropdown-menu li:hover > a > i,
.navbar .dropdown-menu li:focus > a > i,
.navbar .dropdown-menu li:active > a > i,
.navbar.navbar-not-transparent .nav > li:not(.btn).on-section > a, 
.navbar.navbar-not-transparent .nav > li.on-section:not(.btn) > a, 
.navbar.navbar-not-transparent .nav > li.on-section:not(.btn):hover > a, 
.navbar.navbar-not-transparent .nav > li.on-section:not(.btn):focus > a, 
.navbar.navbar-not-transparent .nav > li.on-section:not(.btn):active > a, 
body:not(.home) .navbar-default .navbar-nav > .active:not(.btn) > a,
body:not(.home) .navbar-default .navbar-nav > .active:not(.btn) > a:hover,
body:not(.home) .navbar-default .navbar-nav > .active:not(.btn) > a:focus,
.hestia-blogs article:nth-child(6n+1) .category a, a:hover, .card-blog a.moretag:hover, .card-blog a.more-link:hover, .widget a:hover {
    color:#e91e63;
}

.pagination span.current, .pagination span.current:focus, .pagination span.current:hover {
    border-color:#e91e63
}
           
button,
button:hover,           
input[type="button"],
input[type="button"]:hover,
input[type="submit"],
input[type="submit"]:hover,
input#searchsubmit, 
.pagination span.current, 
.pagination span.current:focus, 
.pagination span.current:hover,
.btn.btn-primary,
.btn.btn-primary:link,
.btn.btn-primary:hover, 
.btn.btn-primary:focus, 
.btn.btn-primary:active, 
.btn.btn-primary.active, 
.btn.btn-primary.active:focus, 
.btn.btn-primary.active:hover,
.btn.btn-primary:active:hover, 
.btn.btn-primary:active:focus, 
.btn.btn-primary:active:hover,
.hestia-sidebar-open.btn.btn-rose,
.hestia-sidebar-close.btn.btn-rose,
.hestia-sidebar-open.btn.btn-rose:hover,
.hestia-sidebar-close.btn.btn-rose:hover,
.hestia-sidebar-open.btn.btn-rose:focus,
.hestia-sidebar-close.btn.btn-rose:focus,
.label.label-primary,
.hestia-work .portfolio-item:nth-child(6n+1) .label,
.nav-cart .nav-cart-content .widget .buttons .button {
    background-color: #e91e63;
}

@media (max-width: 768px) { 
    .navbar .navbar-nav .dropdown a .caret {
        background-color: #e91e63;
    }
    
    .navbar-default .navbar-nav>li>a:hover,
    .navbar-default .navbar-nav>li>a:focus,
    .navbar .navbar-nav .dropdown .dropdown-menu li a:hover,
    .navbar .navbar-nav .dropdown .dropdown-menu li a:focus,
    .navbar button.navbar-toggle:hover,
    .navbar .navbar-nav li:hover > a i {
        color: #e91e63;
    }
}

button,
.button,
input[type="submit"], 
input[type="button"], 
.btn.btn-primary,
.hestia-sidebar-open.btn.btn-rose,
.hestia-sidebar-close.btn.btn-rose {
    -webkit-box-shadow: 0 2px 2px 0 rgba(233,30,99,0.14),0 3px 1px -2px rgba(233,30,99,0.2),0 1px 5px 0 rgba(233,30,99,0.12);
    box-shadow: 0 2px 2px 0 rgba(233,30,99,0.14),0 3px 1px -2px rgba(233,30,99,0.2),0 1px 5px 0 rgba(233,30,99,0.12);
}

.card .header-primary, .card .content-primary {
    background: #e91e63;
}
.button:hover,
button:hover,
input[type="submit"]:hover,
input[type="button"]:hover,
input#searchsubmit:hover, 
.pagination span.current, 
.btn.btn-primary:hover, 
.btn.btn-primary:focus, 
.btn.btn-primary:active, 
.btn.btn-primary.active, 
.btn.btn-primary:active:focus, 
.btn.btn-primary:active:hover, 
.hestia-sidebar-open.btn.btn-rose:hover,
.hestia-sidebar-close.btn.btn-rose:hover,
.pagination span.current:hover{
    -webkit-box-shadow: 0 14px 26px -12pxrgba(233,30,99,0.42),0 4px 23px 0 rgba(0,0,0,0.12),0 8px 10px -5px rgba(233,30,99,0.2);
    box-shadow: 0 14px 26px -12px rgba(233,30,99,0.42),0 4px 23px 0 rgba(0,0,0,0.12),0 8px 10px -5px rgba(233,30,99,0.2);
    color: #fff;
}
.form-group.is-focused .form-control {
background-image: -webkit-gradient(linear,left top, left bottom,from(#e91e63),to(#e91e63)),-webkit-gradient(linear,left top, left bottom,from(#d2d2d2),to(#d2d2d2));
    background-image: -webkit-linear-gradient(#e91e63),to(#e91e63),-webkit-linear-gradient(#d2d2d2,#d2d2d2);
    background-image: linear-gradient(#e91e63),to(#e91e63),linear-gradient(#d2d2d2,#d2d2d2);
}
 .navbar:not(.navbar-transparent) .navbar-nav > li:not(.btn) > a:hover,
 body:not(.home) .navbar:not(.navbar-transparent) .navbar-nav > li.active:not(.btn) > a, .navbar:not(.navbar-transparent) .navbar-nav > li:not(.btn) > a:hover i, .navbar .container .nav-cart:hover .nav-cart-icon {
         color:#e91e63}
.hestia-top-bar, .hestia-top-bar .widget.widget_shopping_cart .cart_list {
            background-color: #363537
        }
        .hestia-top-bar .widget .label-floating input[type=search]:-webkit-autofill {
            -webkit-box-shadow: inset 0 0 0px 9999px #363537
        }.hestia-top-bar, .hestia-top-bar .widget .label-floating input[type=search], .hestia-top-bar .widget.widget_search form.form-group:before, .hestia-top-bar .widget.widget_product_search form.form-group:before, .hestia-top-bar .widget.widget_shopping_cart:before {
            color: #ffffff
        } 
        .hestia-top-bar .widget .label-floating input[type=search]{
            -webkit-text-fill-color:#ffffff !important 
        }.hestia-top-bar a, .hestia-top-bar .top-bar-nav li a {
            color: #ffffff
        }.hestia-top-bar a:hover, .hestia-top-bar .top-bar-nav li a:hover {
            color: #eeeeee
        }
.woocommerce-cart table.shop_table th{
        font-size:9px }.added_to_cart.wc-forward, .products .shop-item .added_to_cart{
        font-size:9.8px }.woocommerce.single-product .product .woocommerce-product-rating .star-rating,
    .woocommerce .star-rating,
    .woocommerce .woocommerce-breadcrumb,
    button, input[type="submit"], input[type="button"], .btn,
    .woocommerce .single-product div.product form.cart .button, .woocommerce div#respond input#submit, 
    .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce div#respond input#submit.alt, 
    .woocommerce a.button.alt, body.woocommerce button.button.alt, .woocommerce button.single_add_to_cart_button,
    #secondary div[id^=woocommerce_price_filter] .button, .footer div[id^=woocommerce_price_filter] .button, .tooltip-inner,
    .woocommerce.single-product article.section-text{
        font-size:12px;
    }.woocommerce-cart table.shop_table th {
        font-size:13px;
    }p, ul, li, select, table, .form-group.label-floating label.control-label, 
    .form-group.label-placeholder label.control-label, .copyright,
    .woocommerce .product .card-product div.card-description p,
    #secondary div[id^=woocommerce_layered_nav] ul li a, #secondary div[id^=woocommerce_product_categories] ul li a, 
    .footer div[id^=woocommerce_layered_nav] ul li a, .footer div[id^=woocommerce_product_categories] ul li a,
    #secondary div[id^=woocommerce_price_filter] .price_label, .footer div[id^=woocommerce_price_filter] .price_label,
    .woocommerce ul.product_list_widget li, .footer ul.product_list_widget li, ul.product_list_widget li,
    .woocommerce .woocommerce-result-count,
    .woocommerce div.product div.woocommerce-tabs ul.tabs.wc-tabs li a,
    .variations tr .label,
    .woocommerce.single-product .section-text,
    .woocommerce div.product form.cart .reset_variations,
    .woocommerce.single-product div.product .woocommerce-review-link,
    .woocommerce div.product form.cart a.reset_variations,
    .woocommerce-cart .shop_table .actions .coupon .input-text,
    .woocommerce-cart table.shop_table td.actions input[type=submit],
    .woocommerce .cart-collaterals .cart_totals .checkout-button,
    .form-control,
    .woocommerce-checkout #payment ul.payment_methods li, .woocommerce-checkout #payment ul.payment_methods div, 
    .woocommerce-checkout #payment ul.payment_methods div p, .woocommerce-checkout #payment input[type=submit], .woocommerce-checkout input[type=submit]
    {
        font-size:14px;
    }#secondary div[id^=woocommerce_recent_reviews] .reviewer, .footer div[id^=woocommerce_recent_reviews] .reviewer{
        font-size:15px;
    }.hestia-features .hestia-info p, .hestia-features .info p, .features .hestia-info p, .features .info p,
    .woocommerce-cart table.shop_table .product-name a,
    .woocommerce-checkout .form-row label, .media p{
        font-size:16px;
    }.blog-post .section-text{ font-size:16.8px; }.hestia-about p{
        font-size:17.5px;
    }.carousel span.sub-title, .media .media-heading, .card .footer .stats .fa{
        font-size:18.2px;
    }table > thead > tr > th{ font-size:21px; }@media (max-width: 767px){.woocommerce-cart table.shop_table th{
        font-size:9px }.added_to_cart.wc-forward, .products .shop-item .added_to_cart{
        font-size:9.8px }.woocommerce.single-product .product .woocommerce-product-rating .star-rating,
    .woocommerce .star-rating,
    .woocommerce .woocommerce-breadcrumb,
    button, input[type="submit"], input[type="button"], .btn,
    .woocommerce .single-product div.product form.cart .button, .woocommerce div#respond input#submit, 
    .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce div#respond input#submit.alt, 
    .woocommerce a.button.alt, body.woocommerce button.button.alt, .woocommerce button.single_add_to_cart_button,
    #secondary div[id^=woocommerce_price_filter] .button, .footer div[id^=woocommerce_price_filter] .button, .tooltip-inner,
    .woocommerce.single-product article.section-text{
        font-size:12px;
    }.woocommerce-cart table.shop_table th {
        font-size:13px;
    }p, ul, li, select, table, .form-group.label-floating label.control-label, 
    .form-group.label-placeholder label.control-label, .copyright,
    .woocommerce .product .card-product div.card-description p,
    #secondary div[id^=woocommerce_layered_nav] ul li a, #secondary div[id^=woocommerce_product_categories] ul li a, 
    .footer div[id^=woocommerce_layered_nav] ul li a, .footer div[id^=woocommerce_product_categories] ul li a,
    #secondary div[id^=woocommerce_price_filter] .price_label, .footer div[id^=woocommerce_price_filter] .price_label,
    .woocommerce ul.product_list_widget li, .footer ul.product_list_widget li, ul.product_list_widget li,
    .woocommerce .woocommerce-result-count,
    .woocommerce div.product div.woocommerce-tabs ul.tabs.wc-tabs li a,
    .variations tr .label,
    .woocommerce.single-product .section-text,
    .woocommerce div.product form.cart .reset_variations,
    .woocommerce.single-product div.product .woocommerce-review-link,
    .woocommerce div.product form.cart a.reset_variations,
    .woocommerce-cart .shop_table .actions .coupon .input-text,
    .woocommerce-cart table.shop_table td.actions input[type=submit],
    .woocommerce .cart-collaterals .cart_totals .checkout-button,
    .form-control,
    .woocommerce-checkout #payment ul.payment_methods li, .woocommerce-checkout #payment ul.payment_methods div, 
    .woocommerce-checkout #payment ul.payment_methods div p, .woocommerce-checkout #payment input[type=submit], .woocommerce-checkout input[type=submit]
    {
        font-size:14px;
    }#secondary div[id^=woocommerce_recent_reviews] .reviewer, .footer div[id^=woocommerce_recent_reviews] .reviewer{
        font-size:15px;
    }.hestia-features .hestia-info p, .hestia-features .info p, .features .hestia-info p, .features .info p,
    .woocommerce-cart table.shop_table .product-name a,
    .woocommerce-checkout .form-row label, .media p{
        font-size:16px;
    }.blog-post .section-text{ font-size:16.8px; }.hestia-about p{
        font-size:17.5px;
    }.carousel span.sub-title, .media .media-heading, .card .footer .stats .fa{
        font-size:18.2px;
    }table > thead > tr > th{ font-size:21px; }}@media (max-width: 480px){.woocommerce-cart table.shop_table th{
        font-size:9px }.added_to_cart.wc-forward, .products .shop-item .added_to_cart{
        font-size:9.8px }.woocommerce.single-product .product .woocommerce-product-rating .star-rating,
    .woocommerce .star-rating,
    .woocommerce .woocommerce-breadcrumb,
    button, input[type="submit"], input[type="button"], .btn,
    .woocommerce .single-product div.product form.cart .button, .woocommerce div#respond input#submit, 
    .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce div#respond input#submit.alt, 
    .woocommerce a.button.alt, body.woocommerce button.button.alt, .woocommerce button.single_add_to_cart_button,
    #secondary div[id^=woocommerce_price_filter] .button, .footer div[id^=woocommerce_price_filter] .button, .tooltip-inner,
    .woocommerce.single-product article.section-text{
        font-size:12px;
    }.woocommerce-cart table.shop_table th {
        font-size:13px;
    }p, ul, li, select, table, .form-group.label-floating label.control-label, 
    .form-group.label-placeholder label.control-label, .copyright,
    .woocommerce .product .card-product div.card-description p,
    #secondary div[id^=woocommerce_layered_nav] ul li a, #secondary div[id^=woocommerce_product_categories] ul li a, 
    .footer div[id^=woocommerce_layered_nav] ul li a, .footer div[id^=woocommerce_product_categories] ul li a,
    #secondary div[id^=woocommerce_price_filter] .price_label, .footer div[id^=woocommerce_price_filter] .price_label,
    .woocommerce ul.product_list_widget li, .footer ul.product_list_widget li, ul.product_list_widget li,
    .woocommerce .woocommerce-result-count,
    .woocommerce div.product div.woocommerce-tabs ul.tabs.wc-tabs li a,
    .variations tr .label,
    .woocommerce.single-product .section-text,
    .woocommerce div.product form.cart .reset_variations,
    .woocommerce.single-product div.product .woocommerce-review-link,
    .woocommerce div.product form.cart a.reset_variations,
    .woocommerce-cart .shop_table .actions .coupon .input-text,
    .woocommerce-cart table.shop_table td.actions input[type=submit],
    .woocommerce .cart-collaterals .cart_totals .checkout-button,
    .form-control,
    .woocommerce-checkout #payment ul.payment_methods li, .woocommerce-checkout #payment ul.payment_methods div, 
    .woocommerce-checkout #payment ul.payment_methods div p, .woocommerce-checkout #payment input[type=submit], .woocommerce-checkout input[type=submit]
    {
        font-size:14px;
    }#secondary div[id^=woocommerce_recent_reviews] .reviewer, .footer div[id^=woocommerce_recent_reviews] .reviewer{
        font-size:15px;
    }.hestia-features .hestia-info p, .hestia-features .info p, .features .hestia-info p, .features .info p,
    .woocommerce-cart table.shop_table .product-name a,
    .woocommerce-checkout .form-row label, .media p{
        font-size:16px;
    }.blog-post .section-text{ font-size:16.8px; }.hestia-about p{
        font-size:17.5px;
    }.carousel span.sub-title, .media .media-heading, .card .footer .stats .fa{
        font-size:18.2px;
    }table > thead > tr > th{ font-size:21px; }}.woocommerce-cart table.shop_table th{
        font-size:9px }.added_to_cart.wc-forward, .products .shop-item .added_to_cart{
        font-size:9.8px }.woocommerce.single-product .product .woocommerce-product-rating .star-rating,
    .woocommerce .star-rating,
    .woocommerce .woocommerce-breadcrumb,
    button, input[type="submit"], input[type="button"], .btn,
    .woocommerce .single-product div.product form.cart .button, .woocommerce div#respond input#submit, 
    .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce div#respond input#submit.alt, 
    .woocommerce a.button.alt, body.woocommerce button.button.alt, .woocommerce button.single_add_to_cart_button,
    #secondary div[id^=woocommerce_price_filter] .button, .footer div[id^=woocommerce_price_filter] .button, .tooltip-inner,
    .woocommerce.single-product article.section-text{
        font-size:12px;
    }.woocommerce-cart table.shop_table th {
        font-size:13px;
    }p, ul, li, select, table, .form-group.label-floating label.control-label, 
    .form-group.label-placeholder label.control-label, .copyright,
    .woocommerce .product .card-product div.card-description p,
    #secondary div[id^=woocommerce_layered_nav] ul li a, #secondary div[id^=woocommerce_product_categories] ul li a, 
    .footer div[id^=woocommerce_layered_nav] ul li a, .footer div[id^=woocommerce_product_categories] ul li a,
    #secondary div[id^=woocommerce_price_filter] .price_label, .footer div[id^=woocommerce_price_filter] .price_label,
    .woocommerce ul.product_list_widget li, .footer ul.product_list_widget li, ul.product_list_widget li,
    .woocommerce .woocommerce-result-count,
    .woocommerce div.product div.woocommerce-tabs ul.tabs.wc-tabs li a,
    .variations tr .label,
    .woocommerce.single-product .section-text,
    .woocommerce div.product form.cart .reset_variations,
    .woocommerce.single-product div.product .woocommerce-review-link,
    .woocommerce div.product form.cart a.reset_variations,
    .woocommerce-cart .shop_table .actions .coupon .input-text,
    .woocommerce-cart table.shop_table td.actions input[type=submit],
    .woocommerce .cart-collaterals .cart_totals .checkout-button,
    .form-control,
    .woocommerce-checkout #payment ul.payment_methods li, .woocommerce-checkout #payment ul.payment_methods div, 
    .woocommerce-checkout #payment ul.payment_methods div p, .woocommerce-checkout #payment input[type=submit], .woocommerce-checkout input[type=submit]
    {
        font-size:14px;
    }#secondary div[id^=woocommerce_recent_reviews] .reviewer, .footer div[id^=woocommerce_recent_reviews] .reviewer{
        font-size:15px;
    }.hestia-features .hestia-info p, .hestia-features .info p, .features .hestia-info p, .features .info p,
    .woocommerce-cart table.shop_table .product-name a,
    .woocommerce-checkout .form-row label, .media p{
        font-size:16px;
    }.blog-post .section-text{ font-size:16.8px; }.hestia-about p{
        font-size:17.5px;
    }.carousel span.sub-title, .media .media-heading, .card .footer .stats .fa{
        font-size:18.2px;
    }table > thead > tr > th{ font-size:21px; }@media (max-width: 767px){.woocommerce-cart table.shop_table th{
        font-size:9px }.added_to_cart.wc-forward, .products .shop-item .added_to_cart{
        font-size:9.8px }.woocommerce.single-product .product .woocommerce-product-rating .star-rating,
    .woocommerce .star-rating,
    .woocommerce .woocommerce-breadcrumb,
    button, input[type="submit"], input[type="button"], .btn,
    .woocommerce .single-product div.product form.cart .button, .woocommerce div#respond input#submit, 
    .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce div#respond input#submit.alt, 
    .woocommerce a.button.alt, body.woocommerce button.button.alt, .woocommerce button.single_add_to_cart_button,
    #secondary div[id^=woocommerce_price_filter] .button, .footer div[id^=woocommerce_price_filter] .button, .tooltip-inner,
    .woocommerce.single-product article.section-text{
        font-size:12px;
    }.woocommerce-cart table.shop_table th {
        font-size:13px;
    }p, ul, li, select, table, .form-group.label-floating label.control-label, 
    .form-group.label-placeholder label.control-label, .copyright,
    .woocommerce .product .card-product div.card-description p,
    #secondary div[id^=woocommerce_layered_nav] ul li a, #secondary div[id^=woocommerce_product_categories] ul li a, 
    .footer div[id^=woocommerce_layered_nav] ul li a, .footer div[id^=woocommerce_product_categories] ul li a,
    #secondary div[id^=woocommerce_price_filter] .price_label, .footer div[id^=woocommerce_price_filter] .price_label,
    .woocommerce ul.product_list_widget li, .footer ul.product_list_widget li, ul.product_list_widget li,
    .woocommerce .woocommerce-result-count,
    .woocommerce div.product div.woocommerce-tabs ul.tabs.wc-tabs li a,
    .variations tr .label,
    .woocommerce.single-product .section-text,
    .woocommerce div.product form.cart .reset_variations,
    .woocommerce.single-product div.product .woocommerce-review-link,
    .woocommerce div.product form.cart a.reset_variations,
    .woocommerce-cart .shop_table .actions .coupon .input-text,
    .woocommerce-cart table.shop_table td.actions input[type=submit],
    .woocommerce .cart-collaterals .cart_totals .checkout-button,
    .form-control,
    .woocommerce-checkout #payment ul.payment_methods li, .woocommerce-checkout #payment ul.payment_methods div, 
    .woocommerce-checkout #payment ul.payment_methods div p, .woocommerce-checkout #payment input[type=submit], .woocommerce-checkout input[type=submit]
    {
        font-size:14px;
    }#secondary div[id^=woocommerce_recent_reviews] .reviewer, .footer div[id^=woocommerce_recent_reviews] .reviewer{
        font-size:15px;
    }.hestia-features .hestia-info p, .hestia-features .info p, .features .hestia-info p, .features .info p,
    .woocommerce-cart table.shop_table .product-name a,
    .woocommerce-checkout .form-row label, .media p{
        font-size:16px;
    }.blog-post .section-text{ font-size:16.8px; }.hestia-about p{
        font-size:17.5px;
    }.carousel span.sub-title, .media .media-heading, .card .footer .stats .fa{
        font-size:18.2px;
    }table > thead > tr > th{ font-size:21px; }}@media (max-width: 480px){.woocommerce-cart table.shop_table th{
        font-size:9px }.added_to_cart.wc-forward, .products .shop-item .added_to_cart{
        font-size:9.8px }.woocommerce.single-product .product .woocommerce-product-rating .star-rating,
    .woocommerce .star-rating,
    .woocommerce .woocommerce-breadcrumb,
    button, input[type="submit"], input[type="button"], .btn,
    .woocommerce .single-product div.product form.cart .button, .woocommerce div#respond input#submit, 
    .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce div#respond input#submit.alt, 
    .woocommerce a.button.alt, body.woocommerce button.button.alt, .woocommerce button.single_add_to_cart_button,
    #secondary div[id^=woocommerce_price_filter] .button, .footer div[id^=woocommerce_price_filter] .button, .tooltip-inner,
    .woocommerce.single-product article.section-text{
        font-size:12px;
    }.woocommerce-cart table.shop_table th {
        font-size:13px;
    }p, ul, li, select, table, .form-group.label-floating label.control-label, 
    .form-group.label-placeholder label.control-label, .copyright,
    .woocommerce .product .card-product div.card-description p,
    #secondary div[id^=woocommerce_layered_nav] ul li a, #secondary div[id^=woocommerce_product_categories] ul li a, 
    .footer div[id^=woocommerce_layered_nav] ul li a, .footer div[id^=woocommerce_product_categories] ul li a,
    #secondary div[id^=woocommerce_price_filter] .price_label, .footer div[id^=woocommerce_price_filter] .price_label,
    .woocommerce ul.product_list_widget li, .footer ul.product_list_widget li, ul.product_list_widget li,
    .woocommerce .woocommerce-result-count,
    .woocommerce div.product div.woocommerce-tabs ul.tabs.wc-tabs li a,
    .variations tr .label,
    .woocommerce.single-product .section-text,
    .woocommerce div.product form.cart .reset_variations,
    .woocommerce.single-product div.product .woocommerce-review-link,
    .woocommerce div.product form.cart a.reset_variations,
    .woocommerce-cart .shop_table .actions .coupon .input-text,
    .woocommerce-cart table.shop_table td.actions input[type=submit],
    .woocommerce .cart-collaterals .cart_totals .checkout-button,
    .form-control,
    .woocommerce-checkout #payment ul.payment_methods li, .woocommerce-checkout #payment ul.payment_methods div, 
    .woocommerce-checkout #payment ul.payment_methods div p, .woocommerce-checkout #payment input[type=submit], .woocommerce-checkout input[type=submit]
    {
        font-size:14px;
    }#secondary div[id^=woocommerce_recent_reviews] .reviewer, .footer div[id^=woocommerce_recent_reviews] .reviewer{
        font-size:15px;
    }.hestia-features .hestia-info p, .hestia-features .info p, .features .hestia-info p, .features .info p,
    .woocommerce-cart table.shop_table .product-name a,
    .woocommerce-checkout .form-row label, .media p{
        font-size:16px;
    }.blog-post .section-text{ font-size:16.8px; }.hestia-about p{
        font-size:17.5px;
    }.carousel span.sub-title, .media .media-heading, .card .footer .stats .fa{
        font-size:18.2px;
    }table > thead > tr > th{ font-size:21px; }}.widget h5{ font-size: 21.84px }.carousel h1.hestia-title, .carousel h2.title{ font-size: 67.2px }h1,.page-header.header-small .hestia-title, .page-header.header-small .title,
    .blog-post.blog-post-wrapper .section-text h1{ font-size: 53.2px }h2, .blog-post.blog-post-wrapper .section-text h2, .woocommerce section.related.products h2, .woocommerce.single-product h1.product_title{ font-size: 36.4px }h3, .blog-post.blog-post-wrapper .section-text h3, .woocommerce .cart-collaterals h2{ font-size: 25.55px }h4, .card-blog .card-title, .blog-post.blog-post-wrapper .section-text h4{ font-size: 18.2px }h5, .blog-post.blog-post-wrapper .section-text h5{ font-size: 17.5px }h6, .blog-post.blog-post-wrapper .section-text h6, .card-product .category{ font-size: 12.6px }.archive .page-header.header-small .hestia-title, .blog .page-header.header-small .hestia-title, 
    .search .page-header.header-small .hestia-title, .archive .page-header.header-small .title, 
    .blog .page-header.header-small .title, .search .page-header.header-small .title{ font-size: 44.8px }.woocommerce span.comment-reply-title, .woocommerce.single-product .summary p.price, .woocommerce.single-product .summary .price, .woocommerce.single-product .woocommerce-variation-price span.price{
        font-size: 25.55px
    }@media (max-width: 767px){.widget h5{ font-size: 21.84px }.carousel h1.hestia-title, .carousel h2.title{ font-size: 67.2px }h1,.page-header.header-small .hestia-title, .page-header.header-small .title,
    .blog-post.blog-post-wrapper .section-text h1{ font-size: 53.2px }h2, .blog-post.blog-post-wrapper .section-text h2, .woocommerce section.related.products h2, .woocommerce.single-product h1.product_title{ font-size: 36.4px }h3, .blog-post.blog-post-wrapper .section-text h3, .woocommerce .cart-collaterals h2{ font-size: 25.55px }h4, .card-blog .card-title, .blog-post.blog-post-wrapper .section-text h4{ font-size: 18.2px }h5, .blog-post.blog-post-wrapper .section-text h5{ font-size: 17.5px }h6, .blog-post.blog-post-wrapper .section-text h6, .card-product .category{ font-size: 12.6px }.archive .page-header.header-small .hestia-title, .blog .page-header.header-small .hestia-title, 
    .search .page-header.header-small .hestia-title, .archive .page-header.header-small .title, 
    .blog .page-header.header-small .title, .search .page-header.header-small .title{ font-size: 44.8px }.woocommerce span.comment-reply-title, .woocommerce.single-product .summary p.price, .woocommerce.single-product .summary .price, .woocommerce.single-product .woocommerce-variation-price span.price{
        font-size: 25.55px
    }}@media (max-width: 480px){.widget h5{ font-size: 21.84px }.carousel h1.hestia-title, .carousel h2.title{ font-size: 67.2px }h1,.page-header.header-small .hestia-title, .page-header.header-small .title,
    .blog-post.blog-post-wrapper .section-text h1{ font-size: 53.2px }h2, .blog-post.blog-post-wrapper .section-text h2, .woocommerce section.related.products h2, .woocommerce.single-product h1.product_title{ font-size: 36.4px }h3, .blog-post.blog-post-wrapper .section-text h3, .woocommerce .cart-collaterals h2{ font-size: 25.55px }h4, .card-blog .card-title, .blog-post.blog-post-wrapper .section-text h4{ font-size: 18.2px }h5, .blog-post.blog-post-wrapper .section-text h5{ font-size: 17.5px }h6, .blog-post.blog-post-wrapper .section-text h6, .card-product .category{ font-size: 12.6px }.archive .page-header.header-small .hestia-title, .blog .page-header.header-small .hestia-title, 
    .search .page-header.header-small .hestia-title, .archive .page-header.header-small .title, 
    .blog .page-header.header-small .title, .search .page-header.header-small .title{ font-size: 44.8px }.woocommerce span.comment-reply-title, .woocommerce.single-product .summary p.price, .woocommerce.single-product .summary .price, .woocommerce.single-product .woocommerce-variation-price span.price{
        font-size: 25.55px
    }}
body, .woocommerce .product .card-product .card-description p,
    .woocommerce ul.product_list_widget li a, .footer ul.product_list_widget li a, ul.product_list_widget li a{
        line-height: 21px;
    }.blog-post, ul, ol, .carousel span.sub-title{
        line-height: 25.2px;
    }.media p{
        line-height: 25.6px;
    }h1, .carousel h1.hestia-title, .carousel h2.title{
        line-height: 77.28px;
    }h2{
        line-height: 54.6px;
    }h3{
        line-height: 35.77px;
    }h4, .widget h5, .woocommerce-cart table.shop_table .product-name a{
        line-height: 28.21px;
    }h5, .hestia-about p{
        line-height: 27.125px;
    }h6{
        line-height: 18.9px;
    }.widget ul li{
        line-height: 33.6px;
    }@media (max-width: 767px){body, .woocommerce .product .card-product .card-description p,
    .woocommerce ul.product_list_widget li a, .footer ul.product_list_widget li a, ul.product_list_widget li a{
        line-height: 21px;
    }.blog-post, ul, ol, .carousel span.sub-title{
        line-height: 25.2px;
    }.media p{
        line-height: 25.6px;
    }h1, .carousel h1.hestia-title, .carousel h2.title{
        line-height: 77.28px;
    }h2{
        line-height: 54.6px;
    }h3{
        line-height: 35.77px;
    }h4, .widget h5, .woocommerce-cart table.shop_table .product-name a{
        line-height: 28.21px;
    }h5, .hestia-about p{
        line-height: 27.125px;
    }h6{
        line-height: 18.9px;
    }.widget ul li{
        line-height: 33.6px;
    }}@media (max-width: 480px){body, .woocommerce .product .card-product .card-description p,
    .woocommerce ul.product_list_widget li a, .footer ul.product_list_widget li a, ul.product_list_widget li a{
        line-height: 21px;
    }.blog-post, ul, ol, .carousel span.sub-title{
        line-height: 25.2px;
    }.media p{
        line-height: 25.6px;
    }h1, .carousel h1.hestia-title, .carousel h2.title{
        line-height: 77.28px;
    }h2{
        line-height: 54.6px;
    }h3{
        line-height: 35.77px;
    }h4, .widget h5, .woocommerce-cart table.shop_table .product-name a{
        line-height: 28.21px;
    }h5, .hestia-about p{
        line-height: 27.125px;
    }h6{
        line-height: 18.9px;
    }.widget ul li{
        line-height: 33.6px;
    }}body, .woocommerce .product .card-product .card-description p,
    .woocommerce ul.product_list_widget li a, .footer ul.product_list_widget li a, ul.product_list_widget li a{
        line-height: 21px;
    }.blog-post, ul, ol, .carousel span.sub-title{
        line-height: 25.2px;
    }.media p{
        line-height: 25.6px;
    }h1, .carousel h1.hestia-title, .carousel h2.title{
        line-height: 77.28px;
    }h2{
        line-height: 54.6px;
    }h3{
        line-height: 35.77px;
    }h4, .widget h5, .woocommerce-cart table.shop_table .product-name a{
        line-height: 28.21px;
    }h5, .hestia-about p{
        line-height: 27.125px;
    }h6{
        line-height: 18.9px;
    }.widget ul li{
        line-height: 33.6px;
    }@media (max-width: 767px){body, .woocommerce .product .card-product .card-description p,
    .woocommerce ul.product_list_widget li a, .footer ul.product_list_widget li a, ul.product_list_widget li a{
        line-height: 21px;
    }.blog-post, ul, ol, .carousel span.sub-title{
        line-height: 25.2px;
    }.media p{
        line-height: 25.6px;
    }h1, .carousel h1.hestia-title, .carousel h2.title{
        line-height: 77.28px;
    }h2{
        line-height: 54.6px;
    }h3{
        line-height: 35.77px;
    }h4, .widget h5, .woocommerce-cart table.shop_table .product-name a{
        line-height: 28.21px;
    }h5, .hestia-about p{
        line-height: 27.125px;
    }h6{
        line-height: 18.9px;
    }.widget ul li{
        line-height: 33.6px;
    }}@media (max-width: 480px){body, .woocommerce .product .card-product .card-description p,
    .woocommerce ul.product_list_widget li a, .footer ul.product_list_widget li a, ul.product_list_widget li a{
        line-height: 21px;
    }.blog-post, ul, ol, .carousel span.sub-title{
        line-height: 25.2px;
    }.media p{
        line-height: 25.6px;
    }h1, .carousel h1.hestia-title, .carousel h2.title{
        line-height: 77.28px;
    }h2{
        line-height: 54.6px;
    }h3{
        line-height: 35.77px;
    }h4, .widget h5, .woocommerce-cart table.shop_table .product-name a{
        line-height: 28.21px;
    }h5, .hestia-about p{
        line-height: 27.125px;
    }h6{
        line-height: 18.9px;
    }.widget ul li{
        line-height: 33.6px;
    }}body, .woocommerce .product .card-product .card-description p,
    .woocommerce ul.product_list_widget li a, .footer ul.product_list_widget li a, ul.product_list_widget li a{
        line-height: 21px;
    }.blog-post, ul, ol, .carousel span.sub-title{
        line-height: 25.2px;
    }.media p{
        line-height: 25.6px;
    }h1, .carousel h1.hestia-title, .carousel h2.title{
        line-height: 77.28px;
    }h2{
        line-height: 54.6px;
    }h3{
        line-height: 35.77px;
    }h4, .widget h5, .woocommerce-cart table.shop_table .product-name a{
        line-height: 28.21px;
    }h5, .hestia-about p{
        line-height: 27.125px;
    }h6{
        line-height: 18.9px;
    }.widget ul li{
        line-height: 33.6px;
    }@media (max-width: 767px){body, .woocommerce .product .card-product .card-description p,
    .woocommerce ul.product_list_widget li a, .footer ul.product_list_widget li a, ul.product_list_widget li a{
        line-height: 21px;
    }.blog-post, ul, ol, .carousel span.sub-title{
        line-height: 25.2px;
    }.media p{
        line-height: 25.6px;
    }h1, .carousel h1.hestia-title, .carousel h2.title{
        line-height: 77.28px;
    }h2{
        line-height: 54.6px;
    }h3{
        line-height: 35.77px;
    }h4, .widget h5, .woocommerce-cart table.shop_table .product-name a{
        line-height: 28.21px;
    }h5, .hestia-about p{
        line-height: 27.125px;
    }h6{
        line-height: 18.9px;
    }.widget ul li{
        line-height: 33.6px;
    }}@media (max-width: 480px){body, .woocommerce .product .card-product .card-description p,
    .woocommerce ul.product_list_widget li a, .footer ul.product_list_widget li a, ul.product_list_widget li a{
        line-height: 21px;
    }.blog-post, ul, ol, .carousel span.sub-title{
        line-height: 25.2px;
    }.media p{
        line-height: 25.6px;
    }h1, .carousel h1.hestia-title, .carousel h2.title{
        line-height: 77.28px;
    }h2{
        line-height: 54.6px;
    }h3{
        line-height: 35.77px;
    }h4, .widget h5, .woocommerce-cart table.shop_table .product-name a{
        line-height: 28.21px;
    }h5, .hestia-about p{
        line-height: 27.125px;
    }h6{
        line-height: 18.9px;
    }.widget ul li{
        line-height: 33.6px;
    }}body, .woocommerce .product .card-product .card-description p,
    .woocommerce ul.product_list_widget li a, .footer ul.product_list_widget li a, ul.product_list_widget li a{
        line-height: 21px;
    }.blog-post, ul, ol, .carousel span.sub-title{
        line-height: 25.2px;
    }.media p{
        line-height: 25.6px;
    }h1, .carousel h1.hestia-title, .carousel h2.title{
        line-height: 77.28px;
    }h2{
        line-height: 54.6px;
    }h3{
        line-height: 35.77px;
    }h4, .widget h5, .woocommerce-cart table.shop_table .product-name a{
        line-height: 28.21px;
    }h5, .hestia-about p{
        line-height: 27.125px;
    }h6{
        line-height: 18.9px;
    }.widget ul li{
        line-height: 33.6px;
    }@media (max-width: 767px){body, .woocommerce .product .card-product .card-description p,
    .woocommerce ul.product_list_widget li a, .footer ul.product_list_widget li a, ul.product_list_widget li a{
        line-height: 21px;
    }.blog-post, ul, ol, .carousel span.sub-title{
        line-height: 25.2px;
    }.media p{
        line-height: 25.6px;
    }h1, .carousel h1.hestia-title, .carousel h2.title{
        line-height: 77.28px;
    }h2{
        line-height: 54.6px;
    }h3{
        line-height: 35.77px;
    }h4, .widget h5, .woocommerce-cart table.shop_table .product-name a{
        line-height: 28.21px;
    }h5, .hestia-about p{
        line-height: 27.125px;
    }h6{
        line-height: 18.9px;
    }.widget ul li{
        line-height: 33.6px;
    }}@media (max-width: 480px){body, .woocommerce .product .card-product .card-description p,
    .woocommerce ul.product_list_widget li a, .footer ul.product_list_widget li a, ul.product_list_widget li a{
        line-height: 21px;
    }.blog-post, ul, ol, .carousel span.sub-title{
        line-height: 25.2px;
    }.media p{
        line-height: 25.6px;
    }h1, .carousel h1.hestia-title, .carousel h2.title{
        line-height: 77.28px;
    }h2{
        line-height: 54.6px;
    }h3{
        line-height: 35.77px;
    }h4, .widget h5, .woocommerce-cart table.shop_table .product-name a{
        line-height: 28.21px;
    }h5, .hestia-about p{
        line-height: 27.125px;
    }h6{
        line-height: 18.9px;
    }.widget ul li{
        line-height: 33.6px;
    }}p, .blog-post .section-text p, .hestia-about p, .card-description p, 
    .woocommerce .product .card-product .card-description p{
        margin-bottom: 10px
    }@media (max-width: 767px){p, .blog-post .section-text p, .hestia-about p, .card-description p, 
    .woocommerce .product .card-product .card-description p{
        margin-bottom: 10px
    }}@media (max-width: 480px){p, .blog-post .section-text p, .hestia-about p, .card-description p, 
    .woocommerce .product .card-product .card-description p{
        margin-bottom: 10px
    }}

.title, .title a, 
.card-title, 
.card-title a, 
.info-title,
.info-title a,
.footer-brand, 
.footer-brand a,
.media .media-heading, 
.media .media-heading a,
.hestia-info .info-title, 
.card-blog a.moretag,
.card-blog a.more-link,
.card .author a,
.hestia-about:not(.section-image) h1, .hestia-about:not(.section-image) h2, .hestia-about:not(.section-image) h3, .hestia-about:not(.section-image) h4, .hestia-about:not(.section-image) h5,
aside .widget h5,
aside .widget a,
.woocommerce.archive .blog-post .products .product-category h2 {
    color: #2d3359;
}
.description, .card-description, .footer-big, .hestia-features .hestia-info p, .text-gray, .hestia-about:not(.section-image) p, .hestia-about:not(.section-image) h6 {
    color: #999999;
} 
.header-filter:before {
    background-color: rgba(0,0,0,0.5);
} 
.page-header, .page-header .hestia-title, .page-header .sub-title {
    color: #fff;
} 
.navbar, .navbar.navbar-default {
    background-color: #fff;
}
.navbar.navbar-transparent {
    background-color: transparent;
}
@media( max-width: 768px ) {
    .navbar.navbar-transparent {
        background-color: #fff;
    }
} 
@media( min-width: 768px ) {
     .navbar, 
     .navbar.navbar-default,
     .navbar.navbar-transparent.full-screen-menu .navbar-collapse,
     .navbar.full-screen-menu.navbar-not-transparent button.navbar-toggle,
     .home .navbar.navbar-not-transparent .navbar-nav > .active:not(.btn) > a,
     .navbar-not-transparent .container .nav-cart .nav-cart-icon {
        color: #555;
    }
} 
@media( min-width: 768px ) {
     .navbar.navbar-transparent,
     .navbar.full-screen-menu.navbar-transparent button.navbar-toggle,
     .home .navbar.navbar-transparent .navbar-nav > .active > a,
      .navbar-transparent .container .nav-cart .nav-cart-icon {
        color: #fff;
    }
}.form-group.is-focused .form-control,
div.wpforms-container .wpforms-form .form-group.is-focused .form-control,
.nf-form-cont input:not([type=button]):focus,
.nf-form-cont select:focus,
.nf-form-cont textarea:focus {
background-image: -webkit-gradient(linear,left top, left bottom,from(#e91e63),to(#e91e63)),-webkit-gradient(linear,left top, left bottom,from(#d2d2d2),to(#d2d2d2));
    background-image: -webkit-linear-gradient(#e91e63),to(#e91e63),-webkit-linear-gradient(#d2d2d2,#d2d2d2);
    background-image: linear-gradient(#e91e63),to(#e91e63),linear-gradient(#d2d2d2,#d2d2d2);
}

.navbar.full-screen-menu button.navbar-toggle:hover,
.navbar.navbar-transparent.full-screen-menu .navbar-collapse .navbar-nav > li > a:hover,
.nav-cart .nav-cart-content .widget a:not(.remove):not(.button):hover {
    color: #e91e63 !important;
}

@media( max-width: 768px ) {
    .navbar-default .navbar-nav>li>a:hover,
    .navbar-default .navbar-nav>li>a:focus,
    .navbar .navbar-nav .dropdown .dropdown-menu li a:hover,
    .navbar .navbar-nav .dropdown .dropdown-menu li a:focus,
    .navbar button.navbar-toggle:hover,
    .navbar .navbar-nav li:hover > a i {
        color: #e91e63;
    }
}
</style>
<link rel='stylesheet' id='hestia_fonts-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A300%2C400%2C500%2C700%7CRoboto+Slab%3A400%2C700&#038;subset=latin%2Clatin-ext&#038;ver=1.1.52' type='text/css' media='all' />
<script type='text/javascript' src='https://balboaintheattic.de/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://balboaintheattic.de/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<link rel='https://api.w.org/' href='https://balboaintheattic.de/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://balboaintheattic.de/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://balboaintheattic.de/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9" />
<link rel="canonical" href="https://balboaintheattic.de/terms-conditions/" />
<link rel='shortlink' href='https://balboaintheattic.de/?p=40' />
<link rel="alternate" type="application/json+oembed" href="https://balboaintheattic.de/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fbalboaintheattic.de%2Fterms-conditions%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://balboaintheattic.de/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fbalboaintheattic.de%2Fterms-conditions%2F&#038;format=xml" />
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
        </head>

<body class="page-template-default page page-id-40 blog-post">
    <div 
    class="wrapper">
        <header class="header ">
            <div id="primary" class="boxed-layout-header page-header header-small">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <h1 class="hestia-title">Registration                </div>
            </div>
        </div>
        
        <div data-parallax="active" class="header-filter" style="background-image: url(https://balboaintheattic.de/wp-content/uploads/2017/11/22256415_142036359748566_3695212274293296412_o.jpg);"></div>    </div>
</header>
<div class="main main-raised">
        <div class="blog-post ">
        <div class="container">
            

<article id="post-40" class="section section-text">
    <div class="row">
                <div class="col-md-8 col-md-offset-2 page-content-wrap">

	  <div class="entry-content">
<slave>
</div><!-- .entry-content -->



        </div>
            </div>
</article>
<div class="section section-blog-info">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-12">
                                    </div>
            </div>
        </div>
    </div>
</div>
        </div>
    </div>
                        <footer class="footer footer-black footer-big">
                        <div class="container">
                                                <div class="hestia-bottom-footer-content">
                                <div class="hestia-bottom-footer-content">
            <ul id="menu-primary-menu-1" class="footer-menu hestia-center"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-15"><a href="https://balboaintheattic.de/">Startseite</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-40 current_page_item menu-item-44"><a href="https://balboaintheattic.de/terms-conditions/">Terms &#038; Conditions</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45"><a href="https://balboaintheattic.de/sample-page/">Impressum</a></li>
</ul>                            </div>
                            </div>
                            </div>
                    </footer>
                <div style="display: none">
                    </div>
                </div>
    </div>
<script type='text/javascript' src='https://balboaintheattic.de/wp-includes/js/comment-reply.min.js?ver=4.9'></script>
<script type='text/javascript' src='https://balboaintheattic.de/wp-content/themes/hestia-pro/assets/bootstrap/js/bootstrap.min.js?ver=1.0.1'></script>
<script type='text/javascript' src='https://balboaintheattic.de/wp-content/themes/hestia-pro/assets/js/material.js?ver=1.0.1'></script>
<script type='text/javascript' src='https://balboaintheattic.de/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var hestiaViewcart = {"view_cart_label":"View cart","view_cart_link":""};
/* ]]> */
</script>
<script type='text/javascript' src='https://balboaintheattic.de/wp-content/themes/hestia-pro/assets/js/scripts.js?ver=1.1.52'></script>
<script type='text/javascript' src='https://balboaintheattic.de/wp-content/themes/hestia-pro/assets/js/scripts-pro.js?ver=1.0.1'></script>
<script type='text/javascript' src='https://balboaintheattic.de/wp-content/themes/hestia-pro/assets/js/hammer.min.js?ver=1.0.1'></script>
<script type='text/javascript' src='https://balboaintheattic.de/wp-includes/js/wp-embed.min.js?ver=4.9'></script>
</body>
</html>
