ad_page_contract {
    
    event management registration page
    
    @author Neophytos Demetriou (neophytos@azet.sk)
    @creation-date 2014-10-15
    @last-modified 2014-11-11
    @cvs-id $Id$
} {
    participant_id:integer,optional,notnull
    project_id:integer,notnull
    token:optional,notnull
} -properties {
}


set locale "en_US"

# Check if the Project ID is valid
if {[info exists participant_id]} {
    set registered_p [db_0or1row event_info "select project_name,person_id,event_name, project_cost_center_id, p.project_id, event_url, event_email from event_management_events f, im_projects p, im_event_participants e where p.project_id = :project_id and p.project_id = f.project_id and e.project_id = f.project_id and e.participant_id = :participant_id"]

    if {!$registered_p} {
        ad_returnredirect [export_vars -base "participants-list" -url {project_id}]
    }
    
    set user_id $person_id

} else {
    db_1row event_info "select project_name,event_name, project_cost_center_id, p.project_id, event_url, event_email from event_management_events f, im_projects p where p.project_id = :project_id and p.project_id = f.project_id"
    set user_id [auth::get_user_id]
}


# Get all the other events which are currently active to provide links
set other_events_html ""


############ CHANGE THIS #####################
set page_title "Registration Form"
set context_bar [ad_context_bar [list [export_vars -base "participants-list" -url {project_id}] $project_name] $page_title]
set left_navbar_html ""
set show_context_help_p 0

set form_id "registration_form"

set locale [lang::user::locale -user_id $user_id]
    
if { [exists_and_not_null participant_id] } {
    set mode display
    set email_mode "inform"
    set component_bay_right_html ""
    set component_bay_left_html ""
} else {
    set mode edit
    set email_mode "text"
    set component_bay_right_html ""
    set component_bay_left_html ""
}

ad_form \
    -name $form_id \
    -mode $mode \
    -export [list project_id person_id] \
    -form {

        participant_id:key(acs_object_id_seq)

        {-section basic_info
            {legendtext {[::event_management::mc Basic_Info_Section "Basic Info"]}}}
            
        {email:text(text)
            {label {[::event_management::mc Participant_Email "Email"]}}
        }

        {first_names:text
            {label {[::event_management::mc Participant_First_Name "First Name"]}}}
        
        {last_name:text
            {label {[::event_management::mc Participant_Last_Name "Last Name"]}}}
        
        {-section contact_details
            {legendtext {[::event_management::mc Contact_Details_Section "Contact Details"]}}}
            
        {ha_line1:text
            {label {[::event_management::mc Address_Line_1 "Address Line 1"]}}
            {html {size 45}}
        }
        {ha_city:text
            {label {[::event_management::mc City "City"]}}
            {html {size 30}}
        }
        
        {ha_state:text,optional
            {label {[::event_management::mc State "State"]}}
            {html {size 5}}
        }
        
        {ha_postal_code:text
            {label {[::event_management::mc Postal_code "Postal Code"]}}
            {html {size 10}}
        }
        
        {ha_country_code:text(select)
            {label {[::event_management::mc Country "Country"]}}
            {html {}}
            {options {[im_country_options]}}
        }
        {cell_phone:text
            {label {[::event_management::mc Phone "Cell Phone"]}}}
        
        {-section course_preferences
            {legendtext {[::event_management::mc Course_Registration_Section "Course Information"]}}}

	    {course:text(select)
		{label {[::event_management::mc Course "Course"]}}
		{html {}}
		{options {[event_management_material_options -project_id $project_id -material_type "Course Income" -locale $locale -include_empty]}}
	    }
    }

#    if {[event_management_material_options -project_id $project_id -material_type "Accommodation" -mandatory -locale $locale] ne ""} {
        ad_form -extend -name $form_id -form {
            {-section accommodation_preferences
                {legendtext {[::event_management::mc Accomodation_Registration_Section "Accommodation Information"]}}}
            {accommodation:text(select)
                {label {[::event_management::mc Accommodation "Accommodation"]}}
                {html {}}
                {options {[event_management_material_options -project_id $project_id -material_type "Accommodation" -locale $locale]}}
                {help_text {[::event_management::mc accomm_help "In case you choose a double room you have to provide a roommate with whom you are willing to share a (140cm wide) mattress. We can't guarantee any twin beds will be available!"]}}
            }
            {alternative_accommodation:text(multiselect),multiple,optional
                {label {[::event_management::mc Alternative_Accommodation "Alternative Accommodation"]}}
                {html {}}
                {options {[event_management_material_options -project_id $project_id -material_type "Accommodation"  -locale $locale]}}
                {help_text {[::event_management::mc alt_accomm_help "Please provide us with other accommodation choices you are fine with in case your first choice isn't available. This will increase your chances of coming to our camp."]}}
            }
            {roommates_text:text(textarea),optional
                {label {[::event_management::mc Roommates "Roommates"]}}
                {html "rows 4 cols 45"}
                {help_text {[::event_management::mc roommates_text_help "Comma-separated list of email addresses"]}}
            }
        }
        
        if { [ad_form_new_p -key participant_id] } {
            ad_form -extend -name $form_id -form {
                {accommodation_text:text(textarea),optional
                    {label {[::event_management::mc Accommodation_Comments "Accommodation Comments"]}}
                    {html "rows 4 cols 45"}
                    {help_text {[::event_management::mc bedmate_help "Please let us know if you are fine to share a double bed with another person (male, female), have a special someone whom you want to share your bed with or any other comments regarding accommodation."]}}
                    {html {style "width:300px;"}}
                }
            }
        }
#    }
    

    ad_form -extend -name $form_id -form {
        {-section event_preferences
        {legendtext {[::event_management::mc Event_Preference_Registration_Section "Other Information"]}}}
    }
    
    if {[event_management_material_options -project_id $project_id -material_type "Food Choice" -mandatory -locale $locale] ne ""} {
         ad_form -extend -name $form_id -form {
            {food_choice:text(select)
                {label {[::event_management::mc Food_Choice "Food Choice"]}}
                {html {}}
                {options {[event_management_material_options -project_id $project_id -material_type "Food Choice" -locale $locale]}}
            }
        }
    }

    im_dynfield::append_attributes_to_form \
        -object_type im_event_participant \
        -form_id $form_id \
        -object_id 0 \
        -advanced_filter_p 0

    if { [ad_form_new_p -key participant_id] } {
        set new_request_p 1
        ad_form -extend -name $form_id -form {
            {comments:text(textarea),optional
                {label {[::event_management::mc Comments "Further Comments"]}}
                {html "rows 4 cols 45"}
                {help_text {[::event_management::mc comments_help "Do you have any further comments for us?"]}}
            }
        }
    }
    
    

    ad_form -extend -name $form_id -form {
        {accepted_terms_p:boolean(checkbox)
            {label {[::event_management::mc Terms_and_Conditions "Terms & Conditions"]}}
            {options {{{[::event_management::mc accepted_conditions "I have read and accept the terms and conditions for this event"]} t}}}
            {help_text {[::event_management::mc terms_location "You can find the terms at <a href='${event_url}/terms'>${event_url}/terms</A>"]}}
        }
    } -edit_request {

        set sql "select uc.*,pa.*,p.*,ep.*
             from im_event_participants ep 
             inner join parties pa on (pa.party_id=ep.person_id) 
             inner join persons p on (p.person_id=ep.person_id) 
             inner join users_contact uc on (uc.user_id=ep.person_id)
             where participant_id=:participant_id"

        db_1row event_participant $sql


        set form_elements [template::form::get_elements $form_id]
        foreach element $form_elements {
            if { [info exists $element] } {
                set value [set $element]
                template::element::set_value $form_id $element $value
            }
        }
        set package_id [ad_conn package_id]

    } -edit_data {

       ::event_management::update_participant \
            -participant_id $participant_id \
            -project_id $project_id \
            -email $email \
            -first_names $first_names \
            -last_name $last_name \
            -accepted_terms_p $accepted_terms_p \
            -course $course \
            -level $level \
            -lead_p $lead_p \
            -payment_type 804 \
            -payment_term "80107" \
            -partner_text $partner_text \
            -cell_phone $cell_phone \
            -ha_line1 $ha_line1 \
            -ha_city $ha_city \
            -ha_state $ha_state \
            -ha_postal_code $ha_postal_code \
            -ha_country_code $ha_country_code \
	-accommodation "" \
	-alternative_accommodation "" \
	-food_choice "" \
	-bus_option "" \
	-roommates_text ""

    } -after_submit {
	    ad_returnredirect [export_vars -base "[apm_package_url_from_key intranet-event-management]admin/simple-registration" -url {participant_id person_id project_id token inviter_text}]
    }

