--upgrade-5.0.0.0.6-5.0.0.0.7
SELECT acs_log__debug('/packages/intranet-event-management/sql/postgresql/upgrade/upgrade-5.0.0.0.6-5.0.0.0.7.sql','');

update acs_object_types set id_column = 'participant_id' where object_type = 'im_event_participant';
update acs_object_type_tables set id_column = 'participant_id' where object_type = 'im_event_participant' and table_name = 'im_event_participants';
SELECT im_category_new (21090, 'Event Participant', 'Event Participant Type');
UPDATE acs_object_types 
SET type_category_type='Event Participant Type' 
WHERE object_type='im_event_participant';

create or replace function im_event_participant__new (
    p_person_id             integer,
    p_company_id            integer,
    p_participant_id        integer,
    p_email                 varchar,
    p_first_names           varchar,
    p_last_name             varchar,
    p_roommates_text        varchar,
    p_creation_ip           varchar,
    p_project_id            integer,
    p_course                integer,
    p_accommodation         integer,
    p_alternative_accommodation text,
    p_food_choice           integer
) returns integer as 
$$
declare

    v_person_name               varchar;

begin

    perform im_biz_object__new (
        p_participant_id,
        'im_event_participant',  -- object_type
        CURRENT_TIMESTAMP,          -- creation_date
        null,                       -- creation_user
        p_creation_ip,
        NULL                        -- context_id
    );

    insert into im_event_participants (

        participant_id,

        person_id, 
        company_id,
        roommates_text,
        project_id,
        event_participant_status_id,
        event_participant_type_id,
        validation_mask,
        course,
        accommodation,
        alternative_accommodation,
        food_choice	
    ) values (

        p_participant_id,
        p_person_id, 
        p_company_id,
        p_roommates_text,
        p_project_id,
        82500,              -- Waiting List / event_participant_status_id
        21090,              -- Event Participant
        1,
        p_course,
        p_accommodation,
        p_alternative_accommodation,
        p_food_choice	
    );

    -- Fill-in missing info in the roommates table for this event.
    --
    -- Note: We plan to show a list of roommates which have not registered
    -- for the event so filtering by project_id ensures that we will
    -- not fill-in the info when the person registers for another event
    -- and, ditto for partner_participant_id.

    v_person_name := person__name(p_person_id);

    update im_event_roommates set
        roommate_person_id = p_person_id,
        roommate_id = p_participant_id
    where
        (roommate_email = p_email or lower(roommate_name) = lower(v_person_name))
        and project_id = p_project_id;

    return p_participant_id;
end;
$$ language 'plpgsql';

create or replace function im_person_id_from_email_or_name(
    p_project_id        integer,
    p_email             varchar,
    p_name              varchar
) returns integer as
$$
begin

    return (select ep.person_id
            from im_event_participants ep inner join persons p on (p.person_id=ep.person_id)
            inner join parties pa on (pa.party_id=ep.person_id)
            where project_id=p_project_id and
            email=p_email);

end;
$$ language 'plpgsql';
