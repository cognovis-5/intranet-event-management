--upgrade-5.0.0.0.8-5.0.0.0.9

SELECT acs_log__debug('/packages/intranet-event-management/sql/postgresql/upgrade/upgrade-5.0.0.0.8-5.0.0.0.9.sql','');

alter table im_event_participants add column event_partners_text varchar(1000);

create table im_event_partners (

    participant_id      integer not null
                        constraint im_event_partners__participant_id_fk
                        references im_event_participants(participant_id),

    -- project_id is available via participant_id but convenient to have it here

	project_id			integer not null
                        constraint im_event_participants__project_fk
                        references im_projects(project_id),
    
    event_partner_name       varchar(250),

    event_partner_email      varchar(250),

    event_partner_person_id  integer
                        constraint im_event_partners__person_id_fk
                        references persons(person_id),

    event_partner_id         integer
                        constraint im_event_partners__event_partner_id_fk
                        references im_event_participants(participant_id),

    event_partner_mutual_p   boolean not null default 'f'

);


create or replace function im_event_partner__new (
    p_participant_id    integer,
    p_project_id        integer,
    p_event_partner_email    varchar,
    p_event_partner_name     varchar
) returns boolean as 
$$
declare
    v_event_partner_person_id    integer;
    v_event_partner_id           integer;
    v_event_partner_mutual_p     boolean;
begin

    select im_person_id_from_email_or_name(p_project_id,p_event_partner_email,p_event_partner_name) into v_event_partner_person_id;

    v_event_partner_mutual_p := false;

    if v_event_partner_person_id is not null then

        select participant_id into v_event_partner_id
        from im_event_participants 
        where project_id=p_project_id 
        and person_id=v_event_partner_person_id;

    end if;

    insert into im_event_partners(
        participant_id,
        project_id,
        event_partner_email,
        event_partner_name,
        event_partner_person_id,
        event_partner_id,
        event_partner_mutual_p
    ) values (
        p_participant_id,
        p_project_id,
        p_event_partner_email,
        p_event_partner_name,
        v_event_partner_person_id,
        v_event_partner_id,
        exists (select 1 from im_event_partners where participant_id=v_event_partner_id and event_partner_id=p_participant_id)
    );

    if v_event_partner_id is not null then 
        update im_event_partners 
        set event_partner_mutual_p=true 
        where participant_id=v_event_partner_id 
        and event_partner_id=p_participant_id;
    end if;

    return true;

end;
$$ language 'plpgsql';

create or replace function im_event_partners__html (
    p_project_id        integer,
    p_participant_id    integer,
    p_base_url          varchar
) returns varchar as 
$$
declare

    v_stub_url  varchar;
    v_event_partner  record;
    v_result    varchar;
    
begin

    v_stub_url := p_base_url || '?project_id=' || p_project_id || '&participant_id=';

    v_result := '';

    for v_event_partner in 
        select *, 
            coalesce(event_partner_email,event_partner_name,'event_partner_text') as event_partner_text,
            coalesce(person__name(event_partner_person_id),'person_name') as person_name
        from 
            im_event_partners
        where 
            participant_id=p_participant_id 
    loop

        if v_event_partner.event_partner_id is null then

            if v_event_partner.event_partner_person_id is null then
                -- this used to be the case "no reg & no usr" but we no longer try to match the user accounts by name
                -- as the user we end up choosing might not register for the event and the one we did not (but has the
                -- same name) does register (so it would complicate things further, so we simplify like this for now)
                v_result := v_result || '<div style="color:red;" title="no event registration">';
                v_result := v_result || v_event_partner.event_partner_text || '</div> (no reg)';
            else
                v_result := v_result || '<div style="color:red;" title="no event registration">';
                v_result := v_result || v_event_partner.event_partner_text || '</div> (no reg)';
            end if;
        else
            if v_event_partner.event_partner_mutual_p then
                v_result := v_result || '<a href="' || v_stub_url || v_event_partner.event_partner_id || '">';
                v_result := v_result || v_event_partner.person_name || '</a>';
            else
                v_result := v_result || '<a style="color:green;" href="' || v_stub_url || v_event_partner.event_partner_id || '">';
                v_result := v_result || v_event_partner.person_name || '</a> (not mutual)';
            end if;

        end if;

        v_result := v_result || '<br>';

    end loop;

    return v_result; 

end;
$$ language 'plpgsql';


create or replace function im_event_participant__new (
    p_person_id             integer,
    p_company_id            integer,
    p_participant_id        integer,
    p_email                 varchar,
    p_first_names           varchar,
    p_last_name             varchar,
    p_roommates_text        varchar,
    p_event_partners_text   varchar,
    p_event_participant_type_id integer,
    p_creation_ip           varchar,
    p_project_id            integer,
    p_course                integer,
    p_accommodation         integer,
    p_alternative_accommodation text,
    p_food_choice           integer
) returns integer as 
$$
declare

    v_person_name               varchar;

begin

    perform im_biz_object__new (
        p_participant_id,
        'im_event_participant',  -- object_type
        CURRENT_TIMESTAMP,          -- creation_date
        null,                       -- creation_user
        p_creation_ip,
        NULL                        -- context_id
    );

    insert into im_event_participants (

        participant_id,

        person_id, 
        company_id,
        roommates_text,
        event_partners_text,
        project_id,
        event_participant_status_id,
        event_participant_type_id,
        validation_mask,
        course,
        accommodation,
        alternative_accommodation,
        food_choice 
    ) values (

        p_participant_id,
        p_person_id, 
        p_company_id,
        p_roommates_text,
        p_event_partners_text,
        p_project_id,
        82500,              -- Waiting List / event_participant_status_id
        p_event_participant_type_id,              -- Event Participant
        1,
        p_course,
        p_accommodation,
        p_alternative_accommodation,
        p_food_choice   
    );

    -- Fill-in missing info in the roommates table for this event.
    --
    -- Note: We plan to show a list of roommates which have not registered
    -- for the event so filtering by project_id ensures that we will
    -- not fill-in the info when the person registers for another event
    -- and, ditto for partner_participant_id.

    v_person_name := person__name(p_person_id);

    update im_event_roommates set
        roommate_person_id = p_person_id,
        roommate_id = p_participant_id
    where
        (roommate_email = p_email or lower(roommate_name) = lower(v_person_name))
        and project_id = p_project_id;

    update im_event_partners set
        event_partner_person_id = p_person_id,
        event_partner_id = p_participant_id
    where
        (event_partner_email = p_email or lower(event_partner_name) = lower(v_person_name))
        and project_id = p_project_id;

    return p_participant_id;
end;
$$ language 'plpgsql';

