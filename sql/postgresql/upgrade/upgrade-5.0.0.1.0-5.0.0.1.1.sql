--upgrade-5.0.0.0.8-5.0.0.0.9

SELECT acs_log__debug('/packages/intranet-event-management/sql/postgresql/upgrade/upgrade-5.0.0.1.0-5.0.0.1.1.sql','');

create or replace function im_event_partners__html (
    p_project_id        integer,
    p_participant_id    integer,
    p_base_url          varchar
) returns varchar as 
$$
declare

    v_stub_url  varchar;
    v_event_partner  record;
    v_result    varchar;
    v_status	varchar;
    
begin

    v_stub_url := p_base_url || '?project_id=' || p_project_id || '&participant_id=';

    v_result := '';

    for v_event_partner in 
        select *, 
            coalesce(event_partner_email,event_partner_name,'event_partner_text') as event_partner_text,
            coalesce(person__name(event_partner_person_id),'person_name') as person_name
        from 
            im_event_partners
        where 
            participant_id=p_participant_id 
    loop

        if v_event_partner.event_partner_id is null then

            if v_event_partner.event_partner_person_id is null then
                -- this used to be the case "no reg & no usr" but we no longer try to match the user accounts by name
                -- as the user we end up choosing might not register for the event and the one we did not (but has the
                -- same name) does register (so it would complicate things further, so we simplify like this for now)
                v_result := v_result || '<div style="color:red;" title="no event registration">';
                v_result := v_result || v_event_partner.event_partner_text || '</div> (no reg)';
            else
                v_result := v_result || '<div style="color:red;" title="no event registration">';
                v_result := v_result || v_event_partner.event_partner_text || '</div> (no reg)';
            end if;
        else
            if v_event_partner.event_partner_mutual_p then
                v_result := v_result || '<a href="' || v_stub_url || v_event_partner.event_partner_id || '">';
                v_result := v_result || v_event_partner.person_name || '</a>';
            else
                v_result := v_result || '<a style="color:green;" href="' || v_stub_url || v_event_partner.event_partner_id || '">';
                v_result := v_result || v_event_partner.person_name || '</a> (not mutual)';
            end if;
	    select im_name_from_id(event_participant_status_id) into v_status from im_event_participants where participant_id = v_event_partner.event_partner_id;
	    v_result := v_result || '<br />' || v_status;
        end if;

--        v_result := v_result || '<br>';

    end loop;

    return v_result; 

end;
$$ language 'plpgsql';
