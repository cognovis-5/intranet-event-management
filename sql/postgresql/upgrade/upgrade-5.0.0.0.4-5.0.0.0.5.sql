--upgrade-5.0.0.0.4-5.0.0.0.5
SELECT acs_log__debug('/packages/intranet-event-management/sql/postgresql/upgrade/upgrade-5.0.0.0.4-5.0.0.0.5.sql','');

SELECT  im_component_plugin__new (
        null,                           		-- plugin_id
        'im_component_plugin',                			-- object_type
        now(),                        			-- creation_date
        null,                           		-- creation_user
        null,                           		-- creation_ip
        null,                           		-- context_id
        'Notes', 		-- plugin_name
        'intranet-core',            		-- package_name
        'right',                        		-- location
        '/event-management/admin/registration',      		-- page_url
        null,                           		-- view_name
        0,                              		-- sort_order
        'im_notes_component -object_id $participant_id'  	-- component_tcl
);

