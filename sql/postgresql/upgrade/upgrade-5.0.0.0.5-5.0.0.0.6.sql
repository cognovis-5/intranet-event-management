--upgrade-5.0.0.0.5-5.0.0.0.6
SELECT acs_log__debug('/packages/intranet-event-management/sql/postgresql/upgrade/upgrade-5.0.0.0.5-5.0.0.0.6.sql','');

-- Update the material types to reflect the columns
update im_categories set category = 'accommodation' where category_id = 9002;
update im_categories set category = 'course' where category_id = 9004;
update im_categories set category = 'food_choice' where category_id = 9007; 
