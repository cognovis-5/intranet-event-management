SELECT acs_log__debug('/packages/intranet-event-management/sql/postgresql/upgrade/upgrade-5.0.0.1.1-5.0.0.1.2.sql','');
create or replace function event_management_event__new(
    p_project_id  	   integer,
    p_event_name        varchar,
    p_company_id        integer,
    p_project_nr        varchar,
    p_cost_center_id    integer,
    p_event_url		varchar,
    p_event_email	varchar
) returns boolean as 
$$
declare
    v_project_id        integer;
begin

    select im_project__new(
        p_project_id,               -- project_id
        'im_project',     -- object_type
        now(),              -- creation_date
        null,               -- creation_user
        null,               -- creation_ip
        null,               -- context_id
        p_event_name,   -- project_name
        p_project_nr,       -- project_nr
        p_project_nr,       -- project_path
        null,               -- parent_id
        p_company_id,       -- company_id
        '2520',              -- project_type_id (=Event)
        76                  -- project_status_id (=Open)
      ) into v_project_id;

    update im_projects set
        project_cost_center_id=p_cost_center_id,
        project_lead_id = (select company_contact_id from im_companies where company_id=p_company_id)
    where project_id=v_project_id;

    insert into event_management_events (
        event_url,
	event_name,
	event_email,
        project_id
    ) values (
        p_event_url,
	p_event_name,
	p_event_email,
        v_project_id
    );

    return true;

end;
$$ language 'plpgsql';
