--upgrade-5.0.0.0.7-5.0.0.0.8
SELECT acs_log__debug('/packages/intranet-event-management/sql/postgresql/upgrade/upgrade-5.0.0.0.7-5.0.0.0.8.sql','');
SELECT im_dynfield_attribute_new ('im_project', 'lottery_run_p','Lottery run?', 'checkbox', 'boolean', 't', 10, 'f', 'event_management_events');

-- move the project_id into the event_id

insert into acs_object_type_tables (id_column,object_type,table_name) values ('project_id','im_project','event_management_events');
update acs_object_type_tables set id_column = 'project_id' where object_type = 'event_management_event';
update acs_object_type_tables set id_column = 'project_id' where object_type = 'im_project' and table_name = 'event_management_events';
update acs_object_types set id_column = 'project_id' where object_type = 'event_management_event';

alter table event_management_events drop column event_id;
alter table event_management_events drop column enabled_p;
update im_projects p set project_name = (select event_name from event_management_events e where p.project_id = e.project_id);
alter table event_management_events add primary key (project_id);

-- Move event materials

drop table event_management_event_materials;

create table event_management_event_materials (
    project_id            integer
                        constraint event_management_event_materials__event_id_fk
                        references event_management_events(project_id) on delete cascade,

    material_id         integer
                        constraint event_management_event_materials__material_id_fk
                        references im_materials(material_id),

    capacity            integer,

    -- status is in "Confirmed", "Pending Payment", or "Partially Paid"
    num_confirmed       integer not null default 0,
    
    -- status is in "Registered"
    num_registered      integer not null default 0,

    -- capacity - num_registered
    free_capacity       integer,

    -- capacity - (num_registered + num_confirmed)
    free_confirmed_capacity integer

);

-- New project creation
create or replace function event_management_event__new(
    p_project_id  	   integer,
    p_event_name        varchar,
    p_company_id        integer,
    p_project_nr        varchar,
    p_cost_center_id    integer,
    p_event_url		varchar,
    p_event_email	varchar
) returns boolean as 
$$
declare
    v_project_id        integer;
begin

    select im_project__new(
        p_project_id,               -- project_id
        'im_project',     -- object_type
        now(),              -- creation_date
        null,               -- creation_user
        null,               -- creation_ip
        null,               -- context_id
        p_event_name,   -- project_name
        p_project_nr,       -- project_nr
        p_project_nr,       -- project_path
        null,               -- parent_id
        p_company_id,       -- company_id
        '2520',              -- project_type_id (=Event)
        76                  -- project_status_id (=Open)
      ) into v_project_id;

    update im_projects set
        project_cost_center_id=p_cost_center_id,
        project_lead_id = (select company_contact_id from im_companies where company_id=p_company_id)
    where project_id=v_project_id;

    insert into event_management_events (
        event_url,
	event_email,
        project_id
    ) values (
        p_event_url,
	p_event_email,
        v_project_id
    );

    return true;

end;
$$ language 'plpgsql';

drop function event_management_event__update(integer,varchar,integer,boolean);
DROP FUNCTION event_management_event__name(integer);


create or replace function event_management_event__name(
    p_project_id          integer
) returns varchar as
$$
begin
    return project_name from im_projects where project_id=p_project_id;
end;
$$ language 'plpgsql';

-- one project per event
create or replace function event_management_event__name_from_project_id(
    p_project_id        integer
) returns varchar as
$$
begin
    return project_name from im_projects where project_id=p_project_id;
end;
$$ language 'plpgsql';
