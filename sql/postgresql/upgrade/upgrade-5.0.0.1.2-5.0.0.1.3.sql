SELECT acs_log__debug('/packages/intranet-event-management/sql/postgresql/upgrade/upgrade-5.0.0.1.2-5.0.0.1.3.sql','');

alter table im_event_participants add column event_participant_level_id integer references im_categories;
alter table im_event_participants add column payment_type_id integer references im_categories;

select im_category_new(11520,'Participant Note','Intranet Notes Type');
