
SELECT acs_log__debug('/packages/intranet-event-management/sql/postgresql/upgrade/upgrade-5.0.0.0.9-5.0.0.1.0.sql','');

create or replace function im_person_id_from_email_or_name(
    p_project_id        integer,
    p_email             varchar, 
    p_name              varchar
) returns integer as 
$$
begin

    return (select ep.person_id
            from im_event_participants ep inner join persons p on (p.person_id=ep.person_id)
            inner join parties pa on (pa.party_id=ep.person_id)
            where project_id=p_project_id and
	    email=p_email limit 1);

end;
$$ language 'plpgsql';