

drop function event_management_event__update(integer,varchar,integer,boolean);
drop function event_management_event__new(integer,varchar,integer,varchar,integer,boolean);

select acs_rel__delete(rel_id) from acs_rels where object_id_one in (select cost_id from im_costs where project_id in (select project_id from event_management_events));
delete from im_payments where cost_id in (select cost_id from im_costs where project_id in (select project_id from event_management_events));
delete from im_invoice_items where invoice_id in (select cost_id from im_costs where project_id in (select project_id from event_management_events));
select acs_object__delete(cost_id),im_invoice__delete(cost_id),im_cost__delete(cost_id) from im_costs where project_id in (select project_id from event_management_events);

-- on delete cascade will make sure all event_management_events rows are deleted
-- when the corresponding project row is deleted
select im_project__delete(project_id) from event_management_events;
drop table event_management_event_materials;
drop table event_management_events;

select event_management__drop_type('event_management_event');

