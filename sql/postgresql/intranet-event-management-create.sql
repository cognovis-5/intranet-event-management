-- @author Neophytos Demetriou
-- @creation-date 2014-10-15
-- @last-modified 2014-10-30

\i events-create.sql
\i participants-create.sql

-- Category IDs 82000-82999 reserved for Events

--
-- Event Registration Status
-- (status Options in the normal flow of things)
--

-- if the participant registers he is put onto a waiting list
SELECT im_category_new (82500, 'Waiting List', 'Event Registration Status');

-- this is the status we provide if the participant is accepted from our side into the camp
SELECT im_category_new (82501, 'Confirmed', 'Event Registration Status');

-- this is the status once the participant clicks the link to get the payment information
SELECT im_category_new (82502, 'Pending Payment', 'Event Registration Status');

-- this is the status once the participant has partially paid
SELECT im_category_new (82503, 'Partially Paid', 'Event Registration Status');

-- this is the status once the participant has fully paid
SELECT im_category_new (82504, 'Registered', 'Event Registration Status');

-- this is the status if the system kicks the partipanct out or we cancel the participant. 
-- This only happens if the participant is not registered yet
SELECT im_category_new (82505, 'Refused', 'Event Registration Status');

-- this is if the participant decided not to come anymore
SELECT im_category_new (82506, 'Cancelled', 'Event Registration Status');

SELECT im_category_new (82507, 'Checked In', 'Event Registration Status');
SELECT im_category_new (82508, 'Payment Overdue', 'Event Registration Status');

--
-- Event Participant Level
--

SELECT im_category_new (82550, 'Beginner', 'Event Participant Level');
SELECT im_category_new (82551, 'Intermediate', 'Event Participant Level');
SELECT im_category_new (82552, 'Advanced', 'Event Participant Level');

-- im_biz_object__new is ill-defined in dump for event mangament, 
-- even though it is correct in intranet-biz-objects.sql (intranet-core)


-- auxiliary function for drop scripts
create or replace function event_management__drop_type(p_object_type varchar) 
returns boolean as 
$$
begin

    delete from im_biz_objects where object_id in (select object_id from acs_objects where object_type=p_object_type);
    delete from acs_objects where object_type=p_object_type;
    delete from acs_object_type_tables where object_type=p_object_type;
    delete from im_dynfield_layout_pages where object_type=p_object_type;
    perform acs_object_type__drop_type(p_object_type,true);

    return true;

end;
$$ language 'plpgsql';

create or replace function event_management__im_payment_after_insert_tr()
returns trigger as
$$
declare
    v_record        record;
    v_vat_amount  numeric(12,2);
    v_status_id     integer;
begin

    select cst.cost_id, cst.cost_status_id, cst.amount, cst.paid_amount, participant_id into v_record
    from im_costs cst 
    inner join im_payments pay on (pay.cost_id=cst.cost_id)
    inner join im_event_participants reg on (reg.company_id = cst.customer_id and reg.project_id = cst.project_id)
    where payment_id = new.payment_id;

    if found then

       select sum(round(item_units*price_per_unit*cb.aux_int1/100,2))
         into v_vat_amount
         from im_invoice_items ii, im_categories ca, im_categories cb, im_materials im 
        where invoice_id = v_record.cost_id
          and ca.category_id = material_type_id
          and ii.item_material_id = im.material_id
          and ca.aux_int2 = cb.category_id; 

        -- cost status: paid (=3810)
        -- cost status: partially paid (=3808)
        -- event registration status: registered (=82504)
        -- event registration status: partially paid (=82503)

        if v_record.amount + v_vat_amount = v_record.paid_amount + new.amount then
            v_status_id = 82504;
        else
            v_status_id = 82503;
        end if;

        update im_event_participants 
        set event_participant_status_id=v_status_id 
        where participant_id=v_record.participant_id; 

    end if;

    return new;

end;
$$ language 'plpgsql';

-- For a product like project-open, triggers make it really hard for developers 
-- to understand why values change after submitting something on the page, 
-- as you usually only check the source code and callbacks but not open PSQL and do 
-- queries on the database table to find out which triggers are installed.
--
-- create trigger event_management__im_payment_after_insert_tr
-- after insert on im_payments
-- for each row
-- execute procedure event_management__im_payment_after_insert_tr();

-- Room information

select acs_object_type__create_type (
        'event_management_event_room',      -- object_type
        'Event Management - Event Room',    -- pretty_name
        'Event Management - Event Rooms',   -- pretty_plural
        'im_biz_object',                -- supertype
        'event_management_event_rooms',     -- table_name
        'room_id',                              -- id_column
        'event_management_event_room',         -- pl/pgsql package_name
        'f',                            -- abstract_p
        null,                           -- type_extension_table
        'event_management_event_room__name'    -- name_method
);

insert into acs_object_type_tables (object_type,table_name,id_column)
values ('event_management_event_room', 'event_management_event_roomss', 'room_id');

update acs_object_types set
    status_type_table = 'event_management_event_rooms',
    status_column = 'event_room_status_id',
    type_column = 'event_room_type_id'
where object_type = 'event_management_event_room';

create table event_management_event_rooms (
    room_id             integer not null
                        constraint event_management_event_rooms_pk
                        primary key,
    room_name           varchar(100) not null,
    room_material_id    integer not null
                        constraint event_management_event_rooms_material_fk
                        references im_materials(material_id),
    sleeping_spots      integer not null,
    single_beds         integer,
    double_beds         integer,
    additional_beds     integer,
    toilet_p            boolean,
    bath_p              boolean,
    description         text
);

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
    where lower(table_name) = 'im_event_participants'
    and lower(column_name) = 'room_id';
    IF 0 != v_count THEN return 0; END IF;

    alter table im_event_participants
    add column room_id            integer
    constraint im_event_participants_room_id_fk
    references event_management_event_rooms(room_id);

    return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();
