-- @author Neophytos Demetriou
-- @creation-date 2014-10-30
-- @last-modified 2014-11-04

select acs_object_type__create_type (
        'im_event_participant',      -- object_type
        'Event Management - Event Participant',    -- pretty_name
        'Event Management - Event Participants',   -- pretty_plural
        'im_biz_object',                -- supertype
        'im_event_participants',     -- table_name
        'participant_id',      		        -- id_column
        'event_participant', 	    -- pl/pgsql package_name
        'f',                            -- abstract_p
        null,                           -- type_extension_table
        'im_event_participant__name'    -- name_method
);

insert into acs_object_type_tables (object_type,table_name,id_column)
values ('im_event_participant', 'im_event_participants', 'participant_id');

update acs_object_types set
    status_type_table = 'im_event_participants',
    status_column = 'event_participant_status_id',
    type_column = 'event_participant_type_id'
where object_type = 'im_event_participant';

---insert into im_biz_object_urls (object_type, url_type, url) values (
---'im_project','view','/event-management/admin/participants/view?person_id=');
---insert into im_biz_object_urls (object_type, url_type, url) values (
---'im_project','edit','/event-management/admin/participants/new?project_id=');



create table im_event_participants (
    participant_id      integer not null
                        constraint im_event_participants_pk
                        primary key,

    person_id			integer not null
                        constraint im_event_participants_person_id_fk
                        references persons(person_id),

    company_id          integer not null
                        constraint im_event_participants_company_id_fk
                        references im_companies(company_id),

    invoice_id          integer
                        constraint im_event_participants_invoice_id_fk
                        references im_invoices(invoice_id),

    -- one project per event 
    
    project_id		integer not null
    	                constraint im_event_participants__project_fk
                        references im_projects(project_id),

    -- Event Management - Event Registration Status
    -- tracks the status of the participant for that event

    event_participant_status_id     integer not null 
                                    constraint im_event_participants__status_fk 
                                    references im_categories(category_id),

    -- Intranet Project Type, e.g. SCC, BCC, or Castle Camp

    event_participant_type_id     integer not null 
                                  constraint im_event_participants__type_fk 
                                  references im_categories(category_id),

    validation_mask             integer not null,

    roommates_text      varchar(1000),

    
    accommodation       integer
                        constraint im_event_participants__accommodation_fk
                        references im_materials(material_id),
    alternative_accommodation text,
    sort_order            integer,
    food_choice         integer
                        constraint im_event_participants__food_choice_fk
                        references im_materials(material_id),
			
    course              integer
                        constraint im_event_participants__course_fk
                    references im_materials(material_id),
    
    confirmation_date   timestamptz
);

create table im_event_roommates (

    participant_id      integer not null
                        constraint im_event_roommates__participant_id_fk
                        references im_event_participants(participant_id),

    -- project_id is available via participant_id but convenient to have it here

	project_id			integer not null
                        constraint im_event_participants__project_fk
                        references im_projects(project_id),
    
    roommate_name       varchar(250),

    roommate_email      varchar(250),

    roommate_person_id  integer
                        constraint im_event_roommates__person_id_fk
                        references persons(person_id),

    roommate_id         integer
                        constraint im_event_roommates__roommate_id_fk
                        references im_event_participants(participant_id),

    roommate_mutual_p   boolean not null default 'f'

);


create or replace function im_event_participant__new (
    p_person_id             integer,
    p_company_id            integer,
    p_participant_id        integer,
    p_email                 varchar,
    p_first_names           varchar,
    p_last_name             varchar,
    p_roommates_text        varchar,
    p_creation_ip           varchar,
    p_project_id            integer,
    p_course                integer,
    p_accommodation         integer,
    p_alternative_accommodation text,
    p_food_choice           integer
) returns integer as 
$$
declare

    v_person_name               varchar;

begin

    perform im_biz_object__new (
        p_participant_id,
        'im_event_participant',  -- object_type
        CURRENT_TIMESTAMP,          -- creation_date
        null,                       -- creation_user
        p_creation_ip,
        NULL                        -- context_id
    );

    insert into im_event_participants (

        participant_id,

        person_id, 
        company_id,
        roommates_text,
        project_id,
        event_participant_status_id,
        event_participant_type_id,
        validation_mask,
        course,
        accommodation,
        alternative_accommodation,
        food_choice	
    ) values (

        p_participant_id,
        p_person_id, 
        p_company_id,
        p_roommates_text,
        p_project_id,
        82500,              -- Waiting List / event_participant_status_id
        21090,              -- Event Participant
        1,
        p_course,
        p_accommodation,
        p_alternative_accommodation,
        p_food_choice	
    );

    -- Fill-in missing info in the roommates table for this event.
    --
    -- Note: We plan to show a list of roommates which have not registered
    -- for the event so filtering by project_id ensures that we will
    -- not fill-in the info when the person registers for another event
    -- and, ditto for partner_participant_id.

    v_person_name := person__name(p_person_id);

    update im_event_roommates set
        roommate_person_id = p_person_id,
        roommate_id = p_participant_id
    where
        (roommate_email = p_email or lower(roommate_name) = lower(v_person_name))
        and project_id = p_project_id;

    return p_participant_id;
end;
$$ language 'plpgsql';


create or replace function im_event_participant__update (
    p_participant_id        integer,
    p_email                 varchar,
    p_first_names           varchar,
    p_last_name             varchar,
    p_creation_ip           varchar,
    p_project_id            integer,
    p_course                integer,
    p_accommodation         integer,
    p_alternative_accommodation text,
    p_food_choice           integer    
) returns boolean as
$$
declare

    v_person_id                 integer;
    v_partner_participant_id    integer;

begin

    select person_id into v_person_id
    from im_event_participants
    where participant_id = p_participant_id;

    update persons set
        first_names = p_first_names,
        last_name = p_last_name
    where
        person_id = v_person_id;

    update im_event_participants set
        course              = p_course,
	accommodation 	    = p_accommodation,
    alternative_accommodation = p_alternative_accommodation,
	food_choice	    = p_food_choice
    where participant_id=p_participant_id;

    return true;

end;
$$ language 'plpgsql';


create or replace function im_event_participant__name (
    p_person_id integer
) returns varchar as
$$
begin
    return person__name(p_person_id);
end;
$$ language 'plpgsql';

create or replace function im_person_id_from_email_or_name(
    p_project_id        integer,
    p_email             varchar, 
    p_name              varchar
) returns integer as 
$$
begin

    return (select ep.person_id
            from im_event_participants ep inner join persons p on (p.person_id=ep.person_id)
            inner join parties pa on (pa.party_id=ep.person_id)
            where project_id=p_project_id and
	    email=p_email limit 1);

end;
$$ language 'plpgsql';

create or replace function im_event_roommate__new (
    p_participant_id    integer,
    p_project_id        integer,
    p_roommate_email    varchar,
    p_roommate_name     varchar
) returns boolean as 
$$
declare
    v_roommate_person_id    integer;
    v_roommate_id           integer;
    v_roommate_mutual_p     boolean;
begin

    select im_person_id_from_email_or_name(p_project_id,p_roommate_email,p_roommate_name) into v_roommate_person_id;

    v_roommate_mutual_p := false;

    if v_roommate_person_id is not null then

        select participant_id into v_roommate_id
        from im_event_participants 
        where project_id=p_project_id 
        and person_id=v_roommate_person_id;

    end if;

    insert into im_event_roommates(
        participant_id,
        project_id,
        roommate_email,
        roommate_name,
        roommate_person_id,
        roommate_id,
        roommate_mutual_p
    ) values (
        p_participant_id,
        p_project_id,
        p_roommate_email,
        p_roommate_name,
        v_roommate_person_id,
        v_roommate_id,
        exists (select 1 from im_event_roommates where participant_id=v_roommate_id and roommate_id=p_participant_id)
    );

    if v_roommate_id is not null then 
        update im_event_roommates 
        set roommate_mutual_p=true 
        where participant_id=v_roommate_id 
        and roommate_id=p_participant_id;
    end if;

    return true;

end;
$$ language 'plpgsql';

create or replace function im_event_roommates__html (
    p_project_id        integer,
    p_participant_id    integer,
    p_base_url          varchar
) returns varchar as 
$$
declare

    v_stub_url  varchar;
    v_roommate  record;
    v_result    varchar;
    
begin

    v_stub_url := p_base_url || '?project_id=' || p_project_id || '&participant_id=';

    v_result := '';

    for v_roommate in 
        select *, 
            coalesce(roommate_email,roommate_name,'roommate_text') as roommate_text,
            coalesce(person__name(roommate_person_id),'person_name') as person_name
        from 
            im_event_roommates
        where 
            participant_id=p_participant_id 
    loop

        if v_roommate.roommate_id is null then

            if v_roommate.roommate_person_id is null then
                -- this used to be the case "no reg & no usr" but we no longer try to match the user accounts by name
                -- as the user we end up choosing might not register for the event and the one we did not (but has the
                -- same name) does register (so it would complicate things further, so we simplify like this for now)
                v_result := v_result || '<div style="color:red;" title="no event registration">';
                v_result := v_result || v_roommate.roommate_text || '</div> (no reg)';
            else
                v_result := v_result || '<div style="color:red;" title="no event registration">';
                v_result := v_result || v_roommate.roommate_text || '</div> (no reg)';
            end if;
        else
            if v_roommate.roommate_mutual_p then
                v_result := v_result || '<a href="' || v_stub_url || v_roommate.roommate_id || '">';
                v_result := v_result || v_roommate.person_name || '</a>';
            else
                v_result := v_result || '<a style="color:green;" href="' || v_stub_url || v_roommate.roommate_id || '">';
                v_result := v_result || v_roommate.person_name || '</a> (not mutual)';
            end if;

        end if;

        v_result := v_result || '<br>';

    end loop;

    return v_result; 

end;
$$ language 'plpgsql';


SELECT im_category_new (21090, 'Event Participant', 'Event Participant Type');

-- Configure when to show dynfields when using the added categories
-- INSERT INTO im_dynfield_type_attribute_map(attribute_id,object_type_id,display_mode)
-- SELECT
--        a.attribute_id,v.column1,'edit' 
-- FROM
--         (VALUES(102),(103),(104)) v,
--         im_dynfield_attributes a,
--         acs_attributes aa               
-- WHERE                                   
--         a.acs_attribute_id = aa.attribute_id
--         and aa.object_type = 'im_project'
--         and also_hard_coded_p = 'f';

-- viewing a dynfield fails if we do not set the type_category_type
UPDATE acs_object_types 
SET type_category_type='Event Participant Type' 
WHERE object_type='im_event_participant';


update im_categories set category = 'accomodation' where category_id = 9002;
update im_categories set category = 'course' where category_id = 9004; 
SELECT im_category_new (9007, 'food_choice', 'Intranet Material Type');

--- TODO: check whether it is best to modify the material_type_id or create new materials
UPDATE im_materials SET material_type_id=9007 WHERE material_id IN (33313,33314);

SELECT im_dynfield_widget__new (
        null,                                   -- widget_id
        'im_dynfield_widget',                   -- object_type
        now(),                                  -- creation_date
        null,                                   -- creation_user
        null,                                   -- creation_ip
        null,                                   -- context_id

        'im_event_participant_course',       -- widget_name
        '#intranet-event-management.Course#',         -- pretty_name
        '#intranet-event-management.Course#',         -- pretty_plural
        10007,                                  -- storage_type_id
        'integer',                              -- acs_datatype
        'generic_sql',                          -- widget
        'integer',                              -- sql_datatype

        --- category 9002 was mispelled as "Accomodation" when it was created in the im_material_types table
        '{custom {sql {SELECT material_id,material_name FROM im_materials WHERE material_type_id=(SELECT material_type_id FROM im_material_types WHERE material_type=''Course Income'')}}}'
);


SELECT im_dynfield_widget__new (
        null,                                   -- widget_id
        'im_dynfield_widget',                   -- object_type
        now(),                                  -- creation_date
        null,                                   -- creation_user
        null,                                   -- creation_ip
        null,                                   -- context_id

        'im_event_participant_accommodation',      -- widget_name
        '#intranet-event-management.Accommodation#',  -- pretty_name
        '#intranet-event-management.Accommodation#',  -- pretty_plural
        10007,                                  -- storage_type_id
        'integer',                              -- acs_datatype
        'generic_sql',                          -- widget
        'integer',                              -- sql_datatype

        --- category 9002 was mispelled as "Accomodation" when it was created in the im_material_types table
        '{custom {sql {SELECT material_id,material_name FROM im_materials WHERE material_type_id=(SELECT material_type_id FROM im_material_types WHERE material_type=''Accomodation'')}}}'
);


SELECT im_dynfield_widget__new (
        null,                                   -- widget_id
        'im_dynfield_widget',                   -- object_type
        now(),                                  -- creation_date
        null,                                   -- creation_user
        null,                                   -- creation_ip
        null,                                   -- context_id

        'im_event_participant_food_choice',        -- widget_name
        '#intranet-event-management.Food_Choice#',    -- pretty_name
        '#intranet-event-management.Food_Choices#',   -- pretty_plural
        10007,                                  -- storage_type_id
        'integer',                              -- acs_datatype
        'generic_sql',                          -- widget
        'integer',                              -- sql_datatype
        '{custom {sql {SELECT material_id,material_name FROM im_materials WHERE material_type_id=(SELECT material_type_id FROM im_material_types WHERE material_type=''Food Choice'')}}}'
);


create or replace function __inline0()
returns boolean as
$$
declare
    v_view_id integer;
begin

    v_view_id = nextval('im_views_seq');

    insert into im_views(
        view_id, 
        view_name, 
        view_status_id, 
        view_type_id, 
        sort_order, 
        view_sql, 
        view_label
    ) values (
        v_view_id,
        'im_event_participants_list',
        null,                       -- view_status_id
        1400,                       -- view_type_id / ObjectList / Intranet DynView Type
        null,                       -- sort_order
        null,                       -- view_sql
        'Event Management - Event Participants List' -- view_label 
    );

    insert into im_view_columns (
        column_id, 
        view_id, 
        group_id, 
        column_name,
        variable_name,
        column_render_tcl, 
        extra_select, 
        extra_where, 
        sort_order, 
        visible_for
    ) values (
        300001, 
        v_view_id,
        NULL,
        'Reg.ID',
        'participant_id',
        '$participant_id',
        '',
        '',
        1,
        ''
    );


    insert into im_view_columns (
        column_id, 
        view_id, 
        group_id, 
        column_name,
        variable_name,
        column_render_tcl, 
        extra_select, 
        extra_where, 
        sort_order, 
        visible_for,
        order_by_clause
    ) values (
        300002, 
        v_view_id,
        NULL,
        'Name',
        'full_name',
        '"<a href=registration?project_id=$project_id&participant_id=$participant_id>$participant_person_name</a><br>$email"',
        'person__name(person_id) as participant_person_name, participant_id',
        '',
        2,
        '',
        'participant_person_name'
    );

    insert into im_view_columns (
        column_id, 
        view_id, 
        group_id, 
        column_name,
        variable_name,
        column_render_tcl, 
        extra_select, 
        extra_where, 
        sort_order, 
        visible_for,
        order_by_clause
    ) values (
        300005,
        v_view_id,
        NULL,
        'Course',
        'course',
        '$course_text',
	'(select material_name from im_materials where material_id = course) as course_text',
        '',
        6,
        '',
        'course_text'
    );


    insert into im_view_columns (
        column_id, 
        view_id, 
        group_id, 
        column_name,
        variable_name,
        column_render_tcl, 
        extra_select, 
        extra_where, 
        sort_order, 
        visible_for,
        order_by_clause
    ) values (
        300006,
        v_view_id,
        NULL,
        'Accomm.',
        'accommodation',
        '[ad_decode $mismatch_accomm_p f $accommodation_text "<font color=red>$accommodation_text</font>"]',
	'(select material_name from im_materials where material_id = accommodation) as accommodation_text',
        '',
        7,
        '',
        'accommodation_text'
    );


    insert into im_view_columns (
        column_id, 
        view_id, 
        group_id, 
        column_name,
        variable_name,
        column_render_tcl, 
        extra_select, 
        extra_where, 
        sort_order, 
        visible_for,
        order_by_clause
    ) values (
        300007,
        v_view_id,
        NULL,
        'Food Choice',
        'food_choice',
        '$food_choice_text',
	'(select material_name from im_materials where material_id = food_choice) as food_choice_text',
        '',
        8,
        '',
        'food_choice_text'
    );


    insert into im_view_columns (
        column_id, 
        view_id, 
        group_id, 
        column_name,
        variable_name,
        column_render_tcl, 
        extra_select, 
        extra_where, 
        sort_order, 
        visible_for,
        datatype
    ) values (
        300012,
        v_view_id,
        NULL,
        'Roommate(s)',
        'roommates',
        '$roommates_html',
        'im_event_roommates__html(project_id,participant_id,''../registration'') as roommates_html',
        '',
        13,
        '',
        'category_pretty'
    );


    insert into im_view_columns (
        column_id, 
        view_id, 
        group_id, 
        column_name,
        variable_name,
        column_render_tcl, 
        extra_select, 
        extra_where, 
        sort_order, 
        visible_for,
        datatype
    ) values (
        300014,
        v_view_id,
        NULL,
        'Status',
        'event_participant_status_id',
        E'[append _out_status_$participant_id <b>$status</b> "<p><a href=\\"/intranet/companies/view?company_id=$company_id\\">company info</a>" <br>$invoice_html"]',
        E'im_name_from_id(event_participant_status_id) as status, (select ''<a href="/intranet-invoices/view?invoice_id='' || invoice_id || ''" title="'' || cost_name || ''">customer invoice</a>'' from im_costs where customer_id=company_id and cost_id=invoice_id order by cost_id desc limit 1) as invoice_html',
        '',
        15,
        '',
        'category_pretty'
    );


    return true;

end;
$$ language 'plpgsql';

select __inline0();
drop function __inline0();

SELECT im_component_plugin__new (
        null,                           -- plugin_id
        'acs_object',                   -- object_type
        now(),                          -- creation_date
        null,                           -- creation_user
        null,                           -- creation_ip
        null,                           -- context_id
        'Intranet Invoice Component',        -- plugin_name
        'intranet-cost',                  -- package_name
        'right',                        -- location
        '/event-management/admin/registration',      -- page_url
        null,                           -- view_name
        12,                             -- sort_order
        'im_costs_base_component $user_id $company_id $project_id'
);

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS integer AS '
DECLARE

    v_object_id integer;
    v_employees integer;
    v_poadmins  integer;

BEGIN
    SELECT group_id INTO v_employees FROM groups where group_name = ''P/O Admins'';

    SELECT group_id INTO v_poadmins FROM groups where group_name = ''Employees'';


    -- Intranet Mail Ticket Component
    SELECT plugin_id INTO v_object_id FROM im_component_plugins WHERE plugin_name = ''Intranet Invoice Component'' AND page_url = ''/event-management/admin/registration'';

    PERFORM im_grant_permission(v_object_id,v_employees,''read'');
    PERFORM im_grant_permission(v_object_id,v_poadmins,''read'');

    RETURN 0;

END;' language 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();


-- Component for roommates
SELECT im_component_plugin__new (
        null,                           -- plugin_id
        'acs_object',                   -- object_type
        now(),                          -- creation_date
        null,                           -- creation_user
        null,                           -- creation_ip
        null,                           -- context_id
        'Intranet Roommate Component',        -- plugin_name
        'intranet-event-management',                  -- package_name
        'right',                        -- location
        '/event-management/admin/registration',      -- page_url
        null,                           -- view_name
        12,                             -- sort_order
        'im_roommate_component -return_url $return_url -participant_id $participant_id'
);

-- Component for projects
SELECT im_component_plugin__new (
        null,                           -- plugin_id
        'acs_object',                   -- object_type
        now(),                          -- creation_date
        null,                           -- creation_user
        null,                           -- creation_ip
        null,                           -- context_id
        'Intranet Mail Participant Component',        -- plugin_name
        'intranet-mail',                  -- package_name
        'right',                        -- location
        '/event-management/admin/registration',      -- page_url
        null,                           -- view_name
        12,                             -- sort_order
        'im_mail_object_component -context_id $project_id -return_url $return_url -mail_url $mail_url -recipient $person_id'
);

