-- @author Neophytos Demetriou
-- @creation-date 2014-10-30
-- @last-modified 2014-10-30

delete from im_view_columns where view_id=(select view_id from im_views where view_name='im_event_participants_list');
delete from im_views where view_name='im_event_participants_list';
delete from im_dynfield_type_attribute_map where attribute_id in (
    select a.attribute_id 
    from im_dynfield_attributes a inner join acs_attributes aa on (a.acs_attribute_id=aa.attribute_id) 
    where object_type='im_event_participant'
); 

select im_dynfield_attribute__del(a.attribute_id)
from im_dynfield_attributes a inner join acs_attributes aa on (a.acs_attribute_id = aa.attribute_id)
where object_type='im_event_participant';

select im_dynfield_widget__del(widget_id) 
from im_dynfield_widgets 
where widget_name in ('event_management_event_participation_accommodation','im_event_participant_food_choice','im_event_participant_bus_options');

drop function im_event_participant__name(integer);
drop function im_event_participant__update(
    integer, varchar, varchar, varchar, varchar,
	integer,
    boolean, varchar, varchar, varchar, varchar, boolean,
    integer, integer, integer, integer,
    integer, integer, integer
);
drop function im_event_participant__new(
    integer,integer, integer, varchar, varchar, varchar, varchar,
	integer,
    boolean, varchar, varchar, varchar, varchar, boolean,
    integer, integer, integer, integer,
    integer, integer, integer
);

drop function event_management_person_id_from_email_or_name(integer,varchar,varchar);
drop function event_management_event_roommate__new(integer,integer,varchar,varchar);

drop table event_management_event_roommates;
drop table im_event_participants;

select event_management__drop_type('im_event_participant');
