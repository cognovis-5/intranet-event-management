-- @author Neophytos Demetriou
-- @creation-date 2014-10-30
-- @last-modified 2014-10-30

select acs_object_type__create_type (
        'event_management_event',          -- object_type
        'Event Management - Event',        -- pretty_name
        'Event Management - Events',       -- pretty_plural
        'im_project',        -- supertype
        'event_management_events',         -- table_name
        'project_id',             -- id_column
        'event_management_event',          -- pl/pgsql package_name
        'f',                    -- abstract_p
        null,                   -- type_extension_table
        'event_management_event__name'     -- name_method
);

SELECT im_category_new ('2520', 'Event', 'Intranet Project Type');

-- add the dynfield attributes and default to consulting project
delete from im_dynfield_type_attribute_map where object_type_id = 2520;

INSERT INTO im_dynfield_type_attribute_map
    (attribute_id, object_type_id, display_mode, help_text,section_heading,default_value,required_p)
select attribute_id,2520,display_mode,help_text,section_heading,default_value,required_p from im_dynfield_type_attribute_map where object_type_id = 2501;

insert into acs_object_type_tables (object_type,table_name,id_column)
values ('event_management_event', 'event_management_events', 'project_id');

update acs_object_types set
        status_type_table = 'im_projects',
        status_column = 'project_status_id',
        type_column = 'project_type_id'
where object_type = 'event_management_event';

---insert into im_biz_object_urls (object_type, url_type, url) values (
---'im_project','view','/event-management/admin/participants/view?person_id=');
---insert into im_biz_object_urls (object_type, url_type, url) values (
---'im_project','edit','/event-management/admin/participants/new?project_id=');

create table event_management_events (
    project_id			integer not null
                        constraint im_event_participants__project_fk
                        references im_projects(project_id) on delete cascade,

    event_url 		varchar(200), 
    event_email       varchar(200)
); 

-- we query for the event_name using project_id
create index event_management_events__project_id__idx on event_management_events(project_id);

create table event_management_event_materials (
    project_id            integer
                        constraint event_management_event_materials__event_id_fk
                        references event_management_events(project_id) on delete cascade,

    material_id         integer
                        constraint event_management_event_materials__material_id_fk
                        references im_materials(material_id),

    capacity            integer,

    -- status is in "Confirmed", "Pending Payment", or "Partially Paid"
    num_confirmed       integer not null default 0,
    
    -- status is in "Registered"
    num_registered      integer not null default 0,

    -- capacity - num_registered
    free_capacity       integer,

    -- capacity - (num_registered + num_confirmed)
    free_confirmed_capacity integer

);

create or replace function event_management_event__new(
    p_project_id  	   integer,
    p_event_name        varchar,
    p_company_id        integer,
    p_project_nr        varchar,
    p_cost_center_id    integer,
    p_event_url		varchar,
    p_event_email	varchar
) returns boolean as 
$$
declare
    v_project_id        integer;
begin

    select im_project__new(
        p_project_id,               -- project_id
        'im_project',     -- object_type
        now(),              -- creation_date
        null,               -- creation_user
        null,               -- creation_ip
        null,               -- context_id
        p_event_name,   -- project_name
        p_project_nr,       -- project_nr
        p_project_nr,       -- project_path
        null,               -- parent_id
        p_company_id,       -- company_id
        '2520',              -- project_type_id (=Event)
        76                  -- project_status_id (=Open)
      ) into v_project_id;

    update im_projects set
        project_cost_center_id=p_cost_center_id,
        project_lead_id = (select company_contact_id from im_companies where company_id=p_company_id)
    where project_id=v_project_id;

    insert into event_management_events (
        event_url,
	event_email,
        project_id
    ) values (
        p_event_url,
	p_event_email,
        v_project_id
    );

    return true;

end;
$$ language 'plpgsql';

create or replace function event_management_event__name(
    p_project_id          integer
) returns varchar as
$$
begin
    return project_name from im_projects where project_id=p_project_id;
end;
$$ language 'plpgsql';

-- one project per event
create or replace function event_management_event__name_from_project_id(
    p_project_id        integer
) returns varchar as
$$
begin
    return project_name from im_projects where project_id=p_project_id;
end;
$$ language 'plpgsql';
