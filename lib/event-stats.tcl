ad_page_contract {

    event_stats.tcl
    @date 2015-03-09
}

db_1row event_info "select project_cost_center_id, p.project_id, project_name as event_name, event_url from event_management_events f, im_projects p where p.project_id = :project_id and p.project_id = f.project_id"

set list_id "stats_list"
set multirow "stats"

# Get the configured Material Types to pre-fill the lists
set material_types [db_list_of_lists material_types "select distinct im_name_from_id(material_type_id), material_type_id from im_materials where material_id in (select material_id from event_management_event_materials where project_id = :project_id)"]


# Get the enabled status for participants
# This is needed to fill the elements
set category_type "Event Registration Status"
set configured_status_ids [db_list event_status_ids "select category_id
	from	im_categories
	where	category_type = :category_type and
		(enabled_p is null or enabled_p = 't')
	order by
		coalesce(sort_order, category_id)"]

# Create the category array
foreach category_id $configured_status_ids {
    set category_name($category_id) [im_category_from_id $category_id]

}

# Generate the lists

multirow create list_names list_name

set db_multirow_extends [list]
foreach x $material_types {
    set material_type [lindex $x 0]
    set material_type_id [lindex $x 1]

    set elements [list material_name [list label "[im_category_from_id $material_type_id]"] dance_role [list label "Dance Role"]]

    set sql_select "select material_name, im_name_from_id(event_participant_type_id) as dance_role"
    foreach category_id $configured_status_ids {
	lappend elements num_$category_id
	lappend elements [list label $category_name($category_id) html {style "text-align:center;"}]

    if {[exists_and_not_null event_participant_level_id]} {
    	append sql_select ",(select count(*) from im_event_participants ep where epmain.event_participant_type_id = ep.event_participant_type_id and $material_type = em.material_id and ep.project_id = :project_id and ep.event_participant_level_id = :event_participant_level_id and event_participant_status_id = $category_id) as num_$category_id"
    } else {
	    append sql_select ",(select count(*) from im_event_participants ep where epmain.event_participant_type_id = ep.event_participant_type_id and $material_type = em.material_id and ep.project_id = :project_id and event_participant_status_id = $category_id) as num_$category_id"
    }


	if {$category_id ne ""} {
	    set total_$category_id 0
	    lappend db_multirow_extends "total_$category_id"
	}
    }

    append sql_select " from im_materials em, im_event_participants epmain where material_type_id = :material_type_id group by epmain.event_participant_type_id, material_id order by material_id"

    set list_name "list_$material_type_id"
    set multirow $list_name
    multirow append list_names $list_name
    template::list::create \
	-name $list_name \
	-multirow $multirow \
	-elements $elements

    
    db_multirow $multirow $multirow $sql_select {
    }
}


set totals [list]
set sql "select count(*) as total, event_participant_status_id as category_id from im_event_participants where project_id = :project_id group by event_participant_status_id" 

if {[exists_and_not_null event_participant_level_id]} {
    set sql "select count(*) as total, event_participant_status_id as category_id from im_event_participants where project_id = :project_id and event_participant_level_id = :event_participant_level_id group by event_participant_status_id"
}
db_foreach totals $sql {
    set total_$category_id $total
}

foreach column $db_multirow_extends {
    set value [set $column]
    if {$value <0} {
	set value 0
    }
    lappend totals $value
}

set cmd "template::multirow append $multirow \"\" \"TOTAL\" [join $totals " "]"
eval $cmd

