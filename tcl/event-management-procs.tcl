ad_proc -public im_project_type_event {} {
    Returns the event project type
} {
   return 2520 
}

ad_proc -public im_cust_event_management_rest_call {
    -valid_vars
    -sql
    -order_by
    { -query_hash_pairs {} }
    { -rest_user_id "0"}

} {
    Handler for GET custom rest calls
    
    @param valid_vars Variables which are valid
    @param sql SQL command to call. Must be ready to accept the \$where_clause and contain object_id and object_name as variables
    @param order_by Field in the SQL command to use for ordering
} {
    array set query_hash $query_hash_pairs
    set base_url "[im_rest_system_url]/intranet-rest"

    if {$rest_user_id eq "0"} {set rest_user_id [ad_conn user_id]}
    
    # Get locate for translation
    set locale [lang::user::locale -user_id $rest_user_id]

    # -------------------------------------------------------
    # Check if there is a where clause specified in the URL and validate the clause.
    set where_clause ""
    if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}


    # -------------------------------------------------------
    # Check if there are "valid_vars" specified in the HTTP header
    # and add these vars to the SQL clause
    set where_clause_list [list]
    foreach v $valid_vars {
        if {[info exists query_hash($v)]} { lappend where_clause_list "$v=$query_hash($v)" }
    }
    if {"" != $where_clause && [llength $where_clause_list] > 0} { append where_clause " and " }
    append where_clause [join $where_clause_list " and "]


    # Check that the query is a valid SQL where clause
    set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
    if {!$valid_sql_where} {
        im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"
        return
    }
    if {"" != $where_clause} { set where_clause "and $where_clause" }

    # Select SQL: Pull out categories.
    append sql " $where_clause order by $order_by"

    # Append pagination "LIMIT $limit OFFSET $start" to the sql.
    set unlimited_sql $sql
    append sql [im_rest_object_type_pagination_sql -query_hash_pairs $query_hash_pairs]

    set value ""
    set result ""
    set obj_ctr 0
    
    db_foreach objects $sql {

        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        set dereferenced_result ""
        foreach v $valid_vars {
            eval "set a $$v"
            regsub -all {\n} $a {\n} a
            regsub -all {\r} $a {} a
            append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
        }
        append result "$komma{\"id\": \"$object_id\", \"object_name\": \"[im_quotejson $object_name]\"$dereferenced_result}" 
        incr obj_ctr
    }

    set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_sencha_tables_rest_call: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
    return
}


ad_proc -public im_event_participant_permissions {user_id participant_id view_var read_var write_var admin_var} {
    Fill the "by-reference" variables read, write and admin
    with the permissions of $user_id on $participant_id.
    
    Currently defaults to grant all permissions. Need to fine grain this later
} {
    upvar $view_var view
    upvar $read_var read
    upvar $write_var write
    upvar $admin_var admin

    set current_user_id $user_id
    set view 1
    set read 1
    set write 1
    set admin 1
}

ad_proc -public im_note_type_skill {} { return 11515 }
ad_proc -public im_note_type_accommodation {} { return 11516 }
ad_proc -public im_note_type_instrument {} { return 11517 }
ad_proc -public im_note_type_volunteer {} { return 11518 }
ad_proc -public im_note_type_participant {} { return 11520 }

ad_proc -public event_management_material_options {
    -project_id:required
    -material_type:required
    { -company_id "" }
    { -locale ""}
    -include_empty:boolean
    -mandatory:boolean
} {
    Returns a list of viable options for display in the registration form
} {
    set material_options [list]
    if {!$mandatory_p} {
        lappend material_options [list "" ""]
    }
    
    if {$company_id eq ""} {
        set company_id [im_company_internal]
    }
    if {$locale eq ""} {
        set locale [lang::user::locale]
    }
    
 
    db_foreach materials "SELECT m.material_id,material_name,itp.price,itp.currency,capacity FROM im_timesheet_prices itp,im_materials m, event_management_event_materials f,event_management_events e, im_projects p WHERE m.material_type_id=(SELECT material_type_id FROM im_material_types WHERE material_type=:material_type) and f.material_id = m.material_id and f.project_id = e.project_id and e.project_id = :project_id and f.capacity >=0 and itp.material_id = m.material_id and itp.company_id = :company_id and p.project_id = e.project_id and (itp.task_type_id is null or itp.task_type_id = p.project_type_id) order by material_nr" {
        if {$capacity >0 || $include_empty_p} {
           set price [expr {double(round(100*$price))/100}]
           set price [lc_numeric [im_numeric_add_trailing_zeros [expr $price+0] 2] "" $locale]
           set material_display "$material_name ($currency $price)"
           lappend material_options [list $material_display $material_id]
        }
    }
    
    return $material_options
    
}

ad_proc -public event_management_accommodation_options {
    -project_id:required
    -mandatory:boolean
    { -company_id "" }
    { -locale ""}
} {
    set material_options [list]
    if {!$mandatory_p} {
        lappend material_options [list "" ""]
    }
    
    if {$company_id eq ""} {
        set company_id [im_company_internal]
    }
    if {$locale eq ""} {
        set locale [lang::user::locale]
    }    
    
    db_foreach materials "select m.material_id,
    coalesce((select count(*) from im_event_participants ep where accommodation = em.material_id and ep.project_id = :project_id and event_participant_status_id = 82500 and person_id not in (select person_id from event_management_event_room_occupants where project_id = :project_id)),0) as num_waitlist,
    coalesce((select sum(er.sleeping_spots) from event_management_event_rooms er where er.room_material_id = em.material_id),1) as capacity,
    coalesce((select count(*) from event_management_event_room_occupants ro, event_management_event_rooms er where er.room_material_id = em.material_id and ro.room_id = er.room_id and ro.project_id =:project_id),0) as occupants,
    material_name,p.price,p.currency 
    FROM event_management_event_materials em
    INNER JOIN event_management_events e on (em.project_id = e.project_id)
    INNER JOIN im_materials m on (em.material_id = m.material_id)
    INNER JOIN im_timesheet_prices p on (em.material_id = p.material_id)
    INNER JOIN im_projects pr on (pr.project_id = e.project_id)
    WHERE em.project_id = e.project_id 
    and e.project_id = :project_id 
    and p.material_id = m.material_id 
    and p.company_id = :company_id 
    and (p.task_type_id is null or p.task_type_id = pr.project_type_id)
    and em.capacity > 0
    and em.material_id in (select material_id from im_materials where material_type_id = 9002)
    order by material_nr" {
        
        # Limit to 10% overcapacity before not allowing this anymore.
#ns_log Notice "CAPA $capacity :: $occupants :: $num_waitlist"
#        if {[expr $capacity * 1.1 - $occupants - $num_waitlist]>0} {
            set price [lc_numeric [im_numeric_add_trailing_zeros [expr $price+0] 2] "" $locale]
            set material_display "$material_name ($currency $price)"
            lappend material_options [list $material_display $material_id]
#        }
    }
    return $material_options
}

ad_proc ::event_management::create_user_if {
    -email 
    -first_names 
    -last_name
    {-company_name ""}
} {
    @author Malte Susdorff (malte.sussdorff@cognovis.de)
    @creation-date 2019-11-04

    Create the user if not existing already
    
    @param email email of the participant
    @param first_names First name of the participant
} {

    set user_id [db_string user_id "select party_id from parties where lower(email)=lower(:email)" -default ""]

    if { $user_id eq {} } {

        set existing_user_p false

        array set creation_info [auth::create_user \
            -first_names $first_names \
            -last_name $last_name \
            -email $email \
            -email_verified_p 1 \
            -nologin]

        # A successful creation_info looks like:
        # username zahir@zunder.com account_status ok creation_status ok
        # generated_pwd_p 0 account_message {} element_messages {}
        # creation_message {} user_id 302913 password D6E09A4E9

        set creation_status "error"
        if { [info exists creation_info(creation_status)] } { 
            set creation_status $creation_info(creation_status)
        }

        if { "ok" != [string tolower $creation_status] } {
            error "[::event_management::mc Error_creating_user "Error creating new user"]:
            [::event_management::mc Error_creating_user_status "Status"]: $creation_status
            \n$creation_info(creation_message)\n$creation_info(element_messages)
            "
        }

        # Extract the user_id from the creation info
        set user_id $creation_info(user_id)

        # Update creation user to allow the creator to admin the user
        # db_dml update_creation_user_id "
        #    update acs_objects
        #    set creation_user = :current_user_id
        #    where object_id = :user_id
        # "

    } else {

        set existing_user_p true

        set sql "update persons set first_names = :first_names, last_name = :last_name where person_id = :user_id"
        db_dml update_names $sql

    }

    # note that upload-contacts-2.tcl only used the participant's name
    # to generate the company name, we refrain from doing so here to
    # avoid any naming conflicts
    if {$company_name eq ""} {
	set company_name "$first_names $last_name"
    }
    
    set company_id [::event_management::create_company_if $user_id $company_name $existing_user_p]

    return [list $user_id $company_id]

}

ad_proc ::event_management::create_participant {
    -participant_id:required
    -project_id:required
    -email:required
    -first_names:required
    -last_name:required
    -accepted_terms_p:required
    -course:required
    -accommodation:required
    -alternative_accommodation:required
    { -accommodation_comments ""}
    -food_choice:required
    -roommates_text:required
    { -event_partners_text ""}
    { -event_participant_type_id ""}
    -cell_phone:required
    { -company_name "" }
    -ha_line1:required
    -ha_city:required
    { -ha_state ""}
    -ha_postal_code:required
    -ha_country_code:required
    { -comments ""}
    -no_callback:boolean
} {
    Create a participant for an event
} {

    set creation_ip [ad_conn peeraddr]

    set email_exists_in_project_p [event_management_participant_email_already_registered -email $email -project_id $project_id]
    set registration_successful_p 0

    if {!$email_exists_in_project_p} {

        db_transaction {

            set user_info_list [::event_management::create_user_if -email $email -first_names $first_names -last_name $last_name -company_name $company_name]

            set person_id [lindex $user_info_list 0]
            set company_id [lindex $user_info_list 1]
            
            ::event_management::set_user_contact_info \
                -user_id $person_id \
                -cell_phone $cell_phone \
                -ha_line1 $ha_line1 \
                -ha_city  $ha_city \
                -ha_state $ha_state \
                -ha_postal_code $ha_postal_code \
                -ha_country_code $ha_country_code

            set sql "
                update im_offices set
                    phone=:cell_phone,
                    address_line1=:ha_line1,
                    address_city=:ha_city,
                    address_state=:ha_state,
                    address_postal_code=:ha_postal_code,
                    address_country_code=lower(:ha_country_code)
                where office_id=(select main_office_id from im_companies where company_id=:company_id)
            "
            db_dml update_company_contact_info $sql
            
            db_exec_plsql insert_participant "select im_event_participant__new(

                :person_id,
                :company_id,
                :participant_id,
                :email,
                :first_names,
                :last_name,
                :roommates_text,
                :event_partners_text,
                :event_participant_type_id,
                :creation_ip,
                :project_id,
                :course,
                :accommodation,
                :alternative_accommodation,
                :food_choice
            )"

            # Take care of roommates
            set roommates_list [lsearch -all -inline -not [split $roommates_text ",|\t\n\r"] {}]

            foreach roommate_text $roommates_list {

                ::event_management::match_name_email $roommate_text roommate_name roommate_email

                db_exec_plsql insert_roommate "select im_event_roommate__new(
                    :participant_id,
                    :project_id,
                    :roommate_email,
                    :roommate_name
                )"

            }


            # Take care of event partners
            set event_partners_list [lsearch -all -inline -not [split $event_partners_text ",|\t\n\r"] {}]

            foreach event_partner_text $event_partners_list {

                ::event_management::match_name_email $event_partner_text event_partner_name event_partner_email

                db_exec_plsql insert_event_partner "select im_event_partner__new(
                    :participant_id,
                    :project_id,
                    :event_partner_email,
                    :event_partner_name
                )"

            }



    	    if {$comments ne ""} {
                set comments [list [string trim $comments] "text/plain"]
                set comment_note_id [db_exec_plsql create_note "
                    SELECT im_note__new(
                        NULL,
                        'im_note',
                        now(),
                        :person_id,
                        '[ad_conn peeraddr]',
                        null,
                        :comments,
                        :participant_id,
                        [im_note_type_participant],
                        [im_note_status_active]
                    )
                    "]
            }
    	
            if {$accommodation_comments ne ""} {
                set accommodation_comments [list [string trim $accommodation_comments] "text/plain"]
                if {$accommodation_comments ne $comments} {
                    set accommodation_note_id [db_exec_plsql create_note "
                        SELECT im_note__new(	  
                        NULL,
                        'im_note',
                        now(),
                        :person_id,
                        '[ad_conn peeraddr]',
                        null,
                        :accommodation_comments,
                        :participant_id,
                        [im_note_type_accommodation],
                        [im_note_status_active]
                    )
                    "]
    	        }
            }
            
            set sort_order [db_string get_max_sort_order "select ((select max(sort_order) from im_event_participants)+1) as sort_order" -default 0]
            if {$sort_order eq "" || $sort_order == 0} {
               set sort_order 1
            } 

            db_dml upda_alt_accom "update im_event_participants set alternative_accommodation = :alternative_accommodation, sort_order =:sort_order where participant_id = :participant_id"

            permission::grant -party_id [im_profile_employees] -object_id $participant_id -privilege admin
            permission::grant -party_id $person_id -object_id $participant_id -privilege write
    	
            set registration_successful_p 1
            
            if {!$no_callback_p} {
                catch {
                    callback im_event_participant_after_create -object_id $participant_id
                } err_msg
            }
        
    
        #   slack::post -channel "participants" -text "New registration in [im_name_from_id $project_id] by $first_names $last_name for [im_material_name -material_id $course]"
        }

    }

    return $registration_successful_p

}

ad_proc -public event_management_roommate_component {
    -participant_id:required
    {-return_url ""}
} {
    Component to display the roommates
} {
    set params [list  [list participant_id $participant_id]  [list return_url ""]  ]
    
    set result [ad_parse_template -params $params "/packages/intranet-event-management/lib/roommates-list"]
    return [string trim $result]    
}

ad_proc -public event_management_participant_randomize {
    -project_id 
    {-only_new_p 0}
    {-event_participant_status_id ""}
    {-cutoff_date ""}
} {
    Randomize the sort order of the participants

    @param cutoff_date Date after which the participants are not randomized anymore but kept at their (end) position
} {
    # Deal out random numbers if we only need to randomize the new entries

    set where_clause "where project_id =:project_id "

    if {$cutoff_date ne ""} {
	append where_clause "and participant_id = object_id and creation_date < to_date(:cutoff_date, 'YYYY-MM-DD HH24:MI')"
	# Add the acs_objects to the where clause. Quick - not nice
	set where_clause ", acs_objects $where_clause"
    }
    
    if {$only_new_p} {
        append where_clause "and sort_order <> participant_id"
        set ctr [db_string max "select max(sort_order) from im_event_participants"]
    } else {
        set ctr 0
    }
    
    if {$event_participant_status_id ne ""} {
        append where_clause "and event_participant_status_id = :event_participant_status_id"
	if {!$only_new_p} {
	    #set the counter to the max sort_order where the current status is not used
	    set ctr [db_string max "select max(sort_order) from im_event_participants where event_participant_status_id <> :event_participant_status_id" -default 0]
	}
    }
    
    set participant_ids [db_list participant "select participant_id from im_event_participants $where_clause order by random()"]
    ds_comment "Counter: $ctr parti... [llength $participant_ids]"
    foreach participant_id $participant_ids {
	incr ctr
	db_dml update "update im_event_participants set sort_order = $ctr where participant_id = :participant_id"
    }
}

ad_proc -public event_management_migrate_alternative_accommodation {
    {-delete_note:boolean}
} {
    Migrate the alternativ accommodation from notes
} {
    db_foreach notes {select note, note_id, object_id as participant_id from im_notes where note like '\{ALTERNATIVE ACCOMMODATION%' or note like '\{Alternative Unterk?nfte%'} {
     set material_ids [list]
     if {[string match "*2p Room*" $note]} {lappend material_ids 33309}
     if {[string match "*2P with*" $note]} {lappend material_ids 39602}
     if {[string match "*3-4 People*" $note]} {lappend material_ids 33308}
     if {[string match "*4+ Room*" $note]} {lappend material_ids 33306}
     if {[string match "*External Accommodation*" $note]} {lappend material_ids 39697}
     db_dml update "update im_event_participants set alternative_accommodation = :material_ids where participant_id = :participant_id"
     if {$delete_note_p} {
         db_dml delete "delete from im_notes where note_id = :note_id"
     }
    } 
}

ad_proc -public event_management_migrate_companies {
    
} {
    Migrate and merge companies
} {
    db_foreach select "select company_id, company_name, company_path,main_office_id from im_companies where company_path like '3%' or company_path like '4%'" {
        set path_elements [split $company_path "_"]
        set old_company_path [join [lrange $path_elements 1 end] "_"]
        set old_company_id [db_string company_id "select company_id from im_companies where company_path = :old_company_path" -default ""]
        if {$old_company_id ne ""} {
            # Update collmex_id      
            set collmex_id [db_string collmex "select collmex_id from im_companies where company_id = :company_id" -default ""]
            if {$collmex_id eq ""} {
                set collmex_id [db_string collmex "select collmex_id from im_companies where company_id = :old_company_id" -default ""]
                db_dml update "update im_companies set collmex_id = :collmex_id where company_id = :company_id"
            }
            
            # Migrate users
            catch {db_dml update_users "update acs_rels set object_id_one = :company_id where rel_type = 'im_company_employee_rel' and object_id_one = :old_company_id"}

            # Migrate invoices
            db_dml update "update im_costs set customer_id = :company_id where customer_id = :old_company_id"
            
        }
    }
}

ad_proc -public event_management_event_room_description {
    -room_id
} {
    Return the room name with the location and the type of room as used in most cases
} {
    if {[db_0or1row room_info "select room_name,e.room_id, office_name, material_name
    from event_management_event_rooms e, im_offices o, im_materials m
    where e.room_office_id = o.office_id
    and e.room_material_id = m.material_id
    and e.room_id = :room_id"]} {
    return "$room_name ($office_name) - $material_name"
    } else {
    return ""
    }
}

ad_proc -public event_management_level_component {
    -participant_id:required
    {-return_url ""}
} {
    Component to display the level information
} {
    set params [list  [list participant_id $participant_id]  [list return_url ""]  ]

    set result [ad_parse_template -params $params "/packages/intranet-event-management/lib/level-info"]
    return [string trim $result]    
}


ad_proc -public event_management_total_already_confirmed_participants {
    {-return_url ""}
} {
    Return total number of already confirmed participants
} {
   
    # Statuses
    set confirmed_status_id [event_management::status::confirmed]
    set registered_status_id [event_management::status::registered]
    set confirmed_status_ids [list $registered_status_id $confirmed_status_id]

    # First we get total number of already acccepted participants
    set total_already_confirmed [db_string get_total_already_confirmed "select count(*) as total_already_confirmed from im_event_participants where event_participant_status_id in ([template::util::tcl_to_sql_list $confirmed_status_ids])" -default 0]
    return $total_already_confirmed
}


ad_proc -public event_management_participant_waiting_spot {
    -participant_id
    {-return_url ""}
} {
    Return spot on waiting_list

    This is the number of people on the waiting list with a lower sort_order than the current participant.
} {

    db_1row get_participant_info "select sort_order as participant_sort_order, project_id from im_event_participants where participant_id =:participant_id"
    set waiting_status_id [event_management::status::waiting_list]

    set num_participants_ahead [db_string num_participants_ahead "select count(*) from im_event_participants where event_participant_status_id = :waiting_status_id and sort_order < :participant_sort_order and project_id = :project_id" -default 0]
    set waiting_spot [expr $num_participants_ahead +1]

    return $waiting_spot

}

ad_proc -public event_management_run_lottery {
    {-num_spots 0}
    -project_id
    {-randomize_p 1}
    {-confirm_p 0}
    {-cutoff_date ""}
} {
    Component to run lottery

    @param cutoff_date Date after which the lottery is ignored
} {
    set affected_participants_text ""
    set affected_participants_list [list]

    set waiting_status [event_management::status::waiting_list]
    
    set total 0

    # Randomize the waiting list
    if {$randomize_p} {
	event_management_participant_randomize -project_id $project_id  -event_participant_status_id $waiting_status
    }
        

    if {$confirm_p} {
	set total_already_confirmed [event_management_total_already_confirmed_participants]
	if {$num_spots >= $total_already_confirmed} {
	    set total_new_participants_to_be_accepted [expr $num_spots-$total_already_confirmed]
	    
	    set confirmed_status [event_management::status::confirmed]
	    
	    set waiting_participants_sql "select p.first_names, p.last_name, ep.participant_id, ep.event_participant_status_id as old_status from im_event_participants ep, persons p where p.person_id = ep.person_id and ep.event_participant_status_id =:waiting_status and ep.project_id = :project_id order by ep.sort_order asc limit :total_new_participants_to_be_accepted"
	    
	    db_foreach show_affected_participants $waiting_participants_sql {
		lappend affected_participants_list $participant_id
	    }
	    
	    
            foreach participant_id $affected_participants_list { 
                db_dml confirm_participant "update im_event_participants set event_participant_status_id =:confirmed_status, confirmation_date = current_timestamp where participant_id=:participant_id"       
                incr total
            }
        } 
    }
    
    # Save to db that we actually executed lottery
    db_dml update_event_lottery_executed "update event_management_events set lottery_run_p = 't' where project_id =:project_id"
    
    return $total
    
}



ad_proc -public event_management_update_participation_status {
    -participant_id
    -status_id
    -no_callback:boolean
} {
    Component to change participation status
} {

    set old_status_id [db_string status "select event_participant_status_id from im_event_participants where participant_id = :participant_id" -default ""]

    # Update the status
    set result [db_dml update_participation_status "update im_event_participants set event_participant_status_id =:status_id where participant_id=:participant_id"]
  
    if {!$no_callback_p} {
        catch {
            callback im_event_participant_after_status_change -participant_id $participant_id -old_status_id $old_status_id -new_status_id $status_id
        } err_msg
    }

    return $result

}



ad_proc -public event_management_auto_cancel {
    {-project_id ""}
    {-return_url ""}
} {
    Auto cancel procedure used with cron
} {
    if {$project_id eq ""} {
        set project_sql ""
    } else {
        set project_sql " and project_id = :project_id"
    }

    # Int variable to just return number of affected users
    set total_affected 0

    set confirmed_status_id [event_management::status::confirmed]
    set canceled_status_id [event_management::status::cancelled]

    set min_days_since_acceptance [parameter::get_from_package_key -package_key "intranet-event-management" -parameter MinDaysSinceAcceptance -default 0]
    if {$min_days_since_acceptance > 0} {
        set sql_participants_to_cancel "select participant_id from im_event_participants where event_participant_status_id =:confirmed_status_id and confirmation_date < NOW() - INTERVAL '$min_days_since_acceptance days' $project_sql "
        
        set participants_to_cancel_list [db_list participants_to_cancel_list $sql_participants_to_cancel]
        foreach participant_id $participants_to_cancel_list {
            event_management_update_participation_status -participant_id $participant_id -status_id $canceled_status_id
            incr total_affected            
        }
    }
    

    return $total_affected
}


ad_proc -public event_management_participant_email_already_registered {
    -email
    -project_id
} {
    Procedure which checks if user was already registered in project.
    This is different than util_email_unique_p which checks if email is already used in whole system 
    Return id of participant or 0 in case email is not yet registered in project

} {

    set email_registered_in_project_p [db_string check_for_email "select participant_id from im_event_participants ep, cc_users u where u.person_id = ep.person_id and u.email = :email and project_id =:project_id " -default 0]
    return $email_registered_in_project_p
}


ad_proc -public event_management_participant_email_from_participant_id {
    -participant_id:required
} {
    Handy procedure that returns email in exchange for participant_id
} {
    set email [db_string get_email_from_participant_id "select email from im_event_participants ep, cc_users u where u.user_id = ep.person_id and ep.participant_id =:participant_id" -default ""]
    return $email
}

