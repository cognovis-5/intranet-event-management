# ad_schedule_proc -thread t 900 ::event_management::mail_notification_system
#ad_schedule_proc -thread t 900 ::event_management::cleanup_text

ad_schedule_proc -schedule_proc ns_schedule_daily -thread t [list 2 30] event_management_auto_cancel

# ---------------------------------------------------------------
# Callbacks
# 
# Generically create callbacks for all "package" object types
# ---------------------------------------------------------------

set object_types {
    event_management_event
    im_event_participant
}

foreach object_type $object_types {

    ad_proc -public -callback ${object_type}_before_create {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
    } {
    This callback allows you to execute action before and after every
    important change of object. Examples:
    - Copy preset values into the object
    - Integrate with external applications via Web services etc.

    @param object_id ID of the $object_type 
    @param status_id Optional status_id category. 
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object types.
    @param type_id Optional type_id of category.
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object states.
    } -

    ad_proc -public -callback ${object_type}_after_create {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
    } {
    This callback allows you to execute action before and after every
    important change of object. Examples:
    - Copy preset values into the object
    - Integrate with external applications via Web services etc.

    @param object_id ID of the $object_type 
    @param status_id Optional status_id category. 
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object types.
    @param type_id Optional type_id of category.
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object states.
    } -



    ad_proc -public -callback ${object_type}_before_update {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
    } {
    This callback allows you to execute action before and after every
    important change of object. Examples:
    - Copy preset values into the object
    - Integrate with external applications via Web services etc.

    @param object_id ID of the $object_type 
    @param status_id Optional status_id category. 
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object types.
    @param type_id Optional type_id of category.
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object states.
    } -

    ad_proc -public -callback ${object_type}_after_update {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
    } {
    This callback allows you to execute action before and after every
    important change of object. Examples:
    - Copy preset values into the object
    - Integrate with external applications via Web services etc.

    @param object_id ID of the $object_type 
    @param status_id Optional status_id category. 
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object types.
    @param type_id Optional type_id of category.
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object states.
    } -



    ad_proc -public -callback ${object_type}_before_delete {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
    } {
    This callback allows you to execute action before and after every
    important change of object. Examples:
    - Copy preset values into the object
    - Integrate with external applications via Web services etc.

    @param object_id ID of the $object_type 
    @param status_id Optional status_id category. 
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object types.
    @param type_id Optional type_id of category.
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object states.
    } -

    ad_proc -public -callback ${object_type}_after_delete {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
    } {
    This callback allows you to execute action before and after every
    important change of object. Examples:
    - Copy preset values into the object
    - Integrate with external applications via Web services etc.

    @param object_id ID of the $object_type 
    @param status_id Optional status_id category. 
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object types.
    @param type_id Optional type_id of category.
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object states.
    } -

    ad_proc -public -callback ${object_type}_view {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
    } {
    This callback tracks acess to the object's main page.

    @param object_id ID of the $object_type 
    @param status_id Optional status_id category. 
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object types.
    @param type_id Optional type_id of category.
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object states.
    } -

    ad_proc -public -callback ${object_type}_form_fill {
        -form_id:required
        -object_id:required
        { -object_type "" }
        { -type_id ""}
        { -page_url "default" }
        { -advanced_filter_p 0 }
        { -include_also_hard_coded_p 0 }
    } {
    This callback tracks acess to the object's main page.

    @param object_id ID of the $object_type 
    @param status_id Optional status_id category. 
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object types.
    @param type_id Optional type_id of category.
           This value is optional. You need to retrieve the status
           from the DB if the value is empty (which should rarely be the case)
           This field allows for quick filtering if the callback 
           implementation is to be executed only on certain object states.
    } -

    ad_proc -public -callback ${object_type}_on_submit {
        -form_id:required
        -object_id:required
    } {
        This callback allows for additional validations and it should be called in the on_submit block of a form

        Usage: 
            form set_error $form_id $error_field $error_message
            break

        @param object_id ID of the $object_type
        @param form_id ID of the form which is being submitted
    } -


}

ad_proc -public -callback event_management_participants_index_filter {
    {-form_id:required}
} {
    This callback is executed after we generated the filter ad_form
    
    This allows you to extend in the uplevel the form with any additional filters you might want to add.

    @param form_id ID of the form to which we want to append filter elements
} - 

