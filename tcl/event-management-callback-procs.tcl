ad_proc -public -callback im_event_participant_after_status_change {
    {-participant_id:required}
    {-old_status_id ""}
    {-new_status_id ""}
} {
    Callback that is executed after any participant status change
} -
